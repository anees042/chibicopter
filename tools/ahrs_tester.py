#!/usr/bin/python

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from math import *
import serial
import time
import math

window = 0
pitch = 0.0
roll = 0.0
yaw = 0.0
w = 1.0 / sqrt(2)
x = 1.0 / sqrt(2)
y = 0.0
z = 0.0

LightAmbient = 0.2, 0.2, 0.2, 1.0
LightDiffuse = 0.5, 0.5, 0.5, 1.0
LightPosition =	1.0, 0.0, 2.0, 1.0

ser = serial.Serial("/dev/ttyUSB0", 115200)
#ser.write("output_euler\n")
#ser.write("auto_on\n")
#ser.write("cal_data: -854 358 -548 316 -478 813\n")

def InitGL(Width, Height):				# We call this right after our OpenGL window is created.
	global LightAmbient, LightDiffuse, LightPosition
	glClearColor(0.0, 0.0, 0.0, 0.0)	# This Will Clear The Background Color To Black
	glClearDepth(1.0)					# Enables Clearing Of The Depth Buffer
	glDepthFunc(GL_LESS)				# The Type Of Depth Test To Do
	glEnable(GL_DEPTH_TEST)				# Enables Depth Testing
	glShadeModel(GL_SMOOTH)				# Enables Smooth Color Shading
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE)
	
	glMatrixMode(GL_PROJECTION)
	glLoadIdentity()					# Reset The Projection Matrix
										# Calculate The Aspect Ratio Of The Window
	gluPerspective(45.0, float(Width)/float(Height), 0.1, 100.0)

	glMatrixMode(GL_MODELVIEW)

	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient)
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse)
	glLightfv(GL_LIGHT1, GL_POSITION,LightPosition)
	glEnable(GL_LIGHT1)
	glEnable(GL_LIGHTING)
	glEnable(GL_COLOR_MATERIAL)

def ReSizeGLScene(Width, Height):
    if Height == 0:
	    Height = 1

    glViewport(0, 0, Width, Height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width)/float(Height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)

def DrawGLScene():
	global pitch, roll, yaw
	global w,x,y,z
	line = ser.readline()
	line.strip("\n\r")
	words = line.split(" ")
	#if(words[0] == "euler:"):
	#	yaw = float(words[3])
	#	pitch = float(words[2])
	#	roll = float(words[1])
	if (words[0] == "quaternion:"):
		w = float(words[1])
		x = float(words[2])
		y = float(words[3])
		z = float(words[4])
	else:
		print line
	wtmp = w
	xtmp = x
	ytmp = -1.0*z
	ztmp = y

	#wtmp = w
	#xtmp = x
	#ytmp = -1.0*z
	#ztmp = y

	x2 = xtmp * xtmp
	y2 = ytmp * ytmp
	z2 = ztmp * ztmp
	xy = xtmp * ytmp
	xz = xtmp * ztmp
	yz = ytmp * ztmp
	wx = wtmp * xtmp
	wy = wtmp * ytmp
	wz = wtmp * ztmp
 
	rotMatrix4 = [0,1,0,0,0,0,-1,0,-1,0,0,0,0,0,0,1]	
	quatToMatrix4 =    [ 1.0 - 2.0 * (y2 + z2), 2.0 * (xy - wz), 2.0 * (xz + wy), 0.0, 2.0 * (xy + wz), 1.0 - 2.0 * (x2 + z2), 2.0 * (yz - wx), 0.0, 2.0 * (xz - wy), 2.0 * (yz + wx), 1.0 - 2.0 * (x2 + y2), 0.0, 0.0, 0.0, 0.0, 1.0] #irgendwo gefunden
	quatToMatrix4inv = [ 1.0 - 2.0 * (y2 + z2), 2.0 * (xy + wz), 2.0 * (xz - wy), 0.0, 2.0 * (xy - wz), 1.0 - 2.0 * (x2 + z2), 2.0 * (yz + wx), 0.0, 2.0 * (xz + wy), 2.0 * (yz - wx), 1.0 - 2.0 * (x2 + y2), 0.0, 0.0, 0.0, 0.0, 1.0]
	print "Rotation quaternion: " + str(wtmp) + " + " + str(xtmp) + "*i + " + str(ytmp) + "*j + " + str(ztmp) + "*k"

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

	#glLoadMatrixf(quatToMatrix4)
	glLoadIdentity()					# Reset The View 
	glTranslatef(0.0, 0.5, -6.0)
	glPushMatrix()

	#glMultMatrixf(rotMatrix4)
	glMultMatrixf(quatToMatrix4inv)

	#glRotatef(yaw, 0.0, 1.0, 0.0)
	#glRotatef(pitch, 0.0, 0.0, 1.0)
	#glRotatef(roll, 1.0, 0.0, 0.0)
	glColor3f(0.0, 0.8, 0.9)
	glutSolidTeapot(1.0)
	glColor3f(1.0,0.0,0.0)
	glutWireTeapot(1.002)	
	glPopMatrix()


	glutSwapBuffers()

def main():
	global window
	glutInit("")
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
	glutInitWindowSize(640, 480)
	glutInitWindowPosition(0, 0)
	window = glutCreateWindow("AHRS Tester")

	glutDisplayFunc (DrawGLScene)
	#glutFullScreen()
	glutIdleFunc(DrawGLScene)
	glutReshapeFunc (ReSizeGLScene)
	InitGL(640, 480)
	glutMainLoop()

print "Hit ESC key to quit."
main()
