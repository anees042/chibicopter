#ifndef _DEBUG_H_
#define _DEBUG_H_
#include "uart_lld.h"

    //#define DEBUG
    #ifdef DEBUG
        #define DEBUG_UART ((BaseSequentialStream*)&SD3)
        #define DEBUG_PRINTLNF(x, ...) (chprintf(DEBUG_UART, "%s\r\n",(x), ## __VA_ARGS__));
        #define DEBUG_PRINTF(x, ...) (chprintf(DEBUG_UART, "%s",(x), ## __VA_ARGS__));
    #else
        #define DEBUG_UART
        #define DEBUG_PRINTLNF(x, ...)
        #define DEBUG_PRINTF(x, ...)
    #endif

#endif /* _DEBUG_H_ */
