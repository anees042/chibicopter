/*
 * quaternion.cpp
 *
 *  Created on: 07.05.2014
 *      Author: tobias
 */

#ifndef QUATERNION_HPP_
#define QUATERNION_HPP_

#include <math.h>

struct EulerAngles {
    float roll,pitch,yaw;
};

class Quaternion {
  public:
    Quaternion() {w=1.0f; x = 0.0f; y=0.0f; z=0.0f;};
    Quaternion(float w, float x, float y, float z) { this->w = w; this->x = x; this->y = y; this->z = z; };
    float w,x,y,z;

    EulerAngles euler(void) const {
      EulerAngles euler;
      //const static double PI_OVER_2 = M_PI * 0.5;
      //const static double EPSILON = 1e-3;
      double sqw, sqx, sqy, sqz;
      //float test =  x*y + z*w;
      // quick conversion to Euler angles to give tilt to user
      sqw = w*w;
      sqx = x*x;
      sqy = y*y;
      sqz = z*w;
        /*
        //if (test > 0.5-EPSILON) { // singularity at north pole
        if ( (test > 0) && ( fabs(test-0.5) < EPSILON ) ){
            euler.pitch = 2 * atan2(x,w);
            euler.yaw = M_PI/2;
            euler.roll = 0;
            return euler;
        }
        // if (test < -0.5+EPSILON) { // singularity at south pole
        if ( (test < 0) && ( fabs(test+0.5) < EPSILON ) ) {
            euler.pitch = -2 * atan2(x,w);
            euler.yaw = - M_PI/2;
            euler.roll = 0;
            return euler;
        }
        euler.roll = atan2(2*y*w-2*x*z , 1 - 2*sqy - 2*sqz);
        euler.yaw = asin(2*test);
        euler.pitch = atan2(2*x*w-2*y*z , 1 - 2*sqx - 2*sqz);
        */


        //funktioniert mit gedrehten koordsystem von sensor
        euler.roll = atan2(2*(y*z + w*x), (sqw-sqx-sqy+sqz));
        euler.pitch = asin(-2*(x*z - w*y));
        euler.yaw = atan2(2*(x*y + w*z), (sqw+sqx-sqy-sqz) );

        //euler.roll = atan2(2*(y*z - w*x), 2*sqw-1+2*sqz);
        //euler.pitch = -atan(2*(x*z+w*y)/ sqrt(1-(2*(x*z+w*y))*(2*(x*z+w*y))));
        //euler.yaw = atan2(2*(x*y - w*z), 2*sqw-1+2*sqx);

        euler.roll = atan2f(2.0*(y*z + w*x), sqw - sqx - sqy + sqz);

        euler.pitch = asinf(2.0 * (w * y - x * z));

        euler.yaw = atan2f(2.0*(x*y + w*z), sqw + sqx - sqy - sqz);


        //euler.roll = atan2(2.0*(x*y + w*z), (sqw+sqx-sqy-sqz));
        //euler.pitch = asin(-2.0*(x*z - w*y)/(sqw+sqx+sqy+sqz));
        //euler.yaw = atan2(2.0*(y*z + w*x),(sqw-sqx-sqy+sqz));
        return euler;


      //euler.yaw = atan2(2*y*w-2*x*z , 1 - 2*sqy - 2*sqz);
      //euler.roll = asin(2*x*y + 2*z*w);
      //euler.pitch = atan2(2*x*w-2*y*z , 1 - 2*sqx - 2*sqz);
      /*
      except when qx*qy + qz*qw = 0.5 (north pole)
      which gives:
      heading = 2 * atan2(x,w)
      bank = 0
      and when qx*qy + qz*qw = -0.5 (south pole)
      which gives:
      heading = -2 * atan2(x,w)
      bank = 0*/
/*
      euler.pitch = asin(2.0 * (w*y - x*z));
      if (PI_OVER_2 - fabs(euler.pitch) > EPSILON) {//geänderterstes fabs hat gefehlt
        euler.yaw = atan2(2.0 * (x*y + w*z),
                         sqx - sqy - sqz + sqw);
        euler.roll = atan2(2.0 * (w*x + y*z),
                         sqw - sqx - sqy + sqz);
      } else {
        // compute heading from local 'down' vector
        euler.roll = atan2(2*y*z - 2*x*w,
                         2*x*z + 2*y*w);
        euler.yaw = 0.0;

        // If facing down, reverse yaw
        if (euler.pitch < 0)
          euler.yaw = M_PI - euler.yaw;
      }*/
      /*
      euler.pitch = atan2(2*y*w - 2*x*z, 1 - 2*y*y - 2*z*z);
      euler.roll = atan2(2*x*w - 2*y*z, 1 - 2*x*x - 2*z*z);
      euler.yaw = asin(2*x*y + 2*z*w);*/
      return euler;
    }

    Quaternion & operator= (Quaternion const& rhs)
    {
      if (this != &rhs)  //oder if (*this != rhs)
        {
          /* kopiere elementweise, oder:*/
        this->w = rhs.w;
        this->x = rhs.x;
        this->y = rhs.y;
        this->z = rhs.z;
        }
        return *this; //Referenz auf das Objekt selbst zurückgeben
    }
};

#endif /* QUATERNION_HPP_ */
 /*! @} */
