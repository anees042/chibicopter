/*
 * SPIDevice.hpp
 *
 *  Created on: 25.06.2014
 *      Author: tobias
 */

#ifndef SPIDEVICE_HPP_
#define SPIDEVICE_HPP_

#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "spi.h"
#include "spi_lld.h"

#include <stdint.h>
#include <stdbool.h>


namespace modules {

  class SPIDevice {
    public:
      SPIDevice(SPIDriver *driver, SPIConfig cfg);

    protected:
      SPIDriver *m_driver;
      const SPIConfig m_spiConfig;
    private:

  };
}

#endif /* SPIDEVICE_HPP_ */
