/*
 * nrf24l01.hpp
 *
 *  Created on: 18.06.2014
 *      Author: tobias
 */

#ifndef NRF24L01_HPP_
#define NRF24L01_HPP_

#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "spi.h"
#include "spi_lld.h"

#include <stdint.h>
#include <stdbool.h>
#include "SPIDevice.hpp"
#include "init_hw.h"

namespace modules {

  class Nrf24l01p {//: protected SPIDevice {
    public:
      union StatusReg
      {
         struct
         {
           uint8_t tx_full   :1;
           uint8_t rx_p_no   :3;
           uint8_t max_rt    :1;
           uint8_t tx_ds     :1;
           uint8_t rx_dr     :1;
           uint8_t reserved  :1;
         };
         uint8_t reg;
      };

      enum class Pa_dbm { RF24_PA_MIN = 0,RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX, RF24_PA_ERROR };
      enum class DataRate { RF24_1MBPS = 0, RF24_2MBPS, RF24_250KBPS };
      enum class CrcLength { RF24_CRC_DISABLED = 0, RF24_CRC_8, RF24_CRC_16 };
      enum class AddressWidth { RF24_3BYTES = 0, RF24_4BYTES, RF24_5BYTES};

      Nrf24l01p(SPIDriver *driver, SPIConfig spiConfig, INIT_HW::IoPin ce);

      void Lock(void);
      void Unlock(void);

      void EnterRx(void);
      void EnterTx(void);
      void EnterStandby(void);

      void PowerUp(void);
      void PowerDown(void);

      void Crc(CrcLength newCrc);
      void InterruptMaxRetries(bool enabled);
      void InterruptTxDataSent(bool enabled);
      void InterruptRxDataReceived(bool enabled);
      void ClearInterruptMaxRetriesFlag(void);
      void ClearInterruptTxDataSentFlag(void);
      void ClearInterruptRxDataReceivedFlag(void);
      void ClearInterruptAllFlags(void);

      void EnableAutoAcknowledgment(uint8_t pipeMask, bool enabled);
      void EnableRxAddresses(uint8_t addressMask, bool enabled);

      void SetupAddressWidth(AddressWidth newWidth);
      void AutomaticRetransmissionDelay(uint8_t delayMask);
      void AutomaticRetransmissionCount(uint8_t countMask);

      void RfChannel(uint8_t newChannel);
      void EnableContinuousCarrier(bool enabled);
      void SetDataRate(DataRate rate);
      void ForcePllLock(bool enabled);
      void RfPower(Pa_dbm newRfPower);
      StatusReg GetStatus(void);
      uint8_t LostPackets(void);
      uint8_t RetransmittedCount(void);
      bool ReceivedPowerDetector(void);

      void RxAddressPipe0(const uint8_t address[5]);
      void RxAddressPipe1(const uint8_t address[5]);
      void RxAddressPipe2(const uint8_t address);
      void RxAddressPipe3(const uint8_t address);
      void RxAddressPipe4(const uint8_t address);
      void RxAddressPipe5(const uint8_t address);
      void TxAddress(const uint8_t address[5]);

      void RxPayloadWidthPipe0(uint8_t numBytes);
      void RxPayloadWidthPipe1(uint8_t numBytes);
      void RxPayloadWidthPipe2(uint8_t numBytes);
      void RxPayloadWidthPipe3(uint8_t numBytes);
      void RxPayloadWidthPipe4(uint8_t numBytes);
      void RxPayloadWidthPipe5(uint8_t numBytes);

      bool FifoTxReuse(void);
      bool FifoTxFull(void);
      bool FifoTxEmpty(void);
      bool FifoRxFull(void);
      bool FifoRxEmpty(void);

      void EnableDynamicPayloadPipe0(bool enabled);
      void EnableDynamicPayloadPipe1(bool enabled);
      void EnableDynamicPayloadPipe2(bool enabled);
      void EnableDynamicPayloadPipe3(bool enabled);
      void EnableDynamicPayloadPipe4(bool enabled);
      void EnableDynamicPayloadPipe5(bool enabled);

      void EnableDynamicPayloadLength(bool enabled);
      void EnablePayloadWithAck(bool enabled);
      void EnableDynamicAck(bool enabled);

    protected:
      SPIDriver *m_driver;
      const SPIConfig m_spiConfig;

      void chipEnable(bool enable) const;
      bool chipEnable(void) const;

      void read_register(const uint8_t reg, const uint8_t len, uint8_t buf[]) const;
      void write_register(const uint8_t reg, const uint8_t len, const uint8_t buf[]) const;

      void read_rx_payload(const uint8_t len, uint8_t buf[]) const;
      void write_tx_payload(const uint8_t len, const uint8_t buf[]) const;

      void flush_tx(void) const;
      void flush_rx(void) const;

      void reuse_tx_pl(void) const;

      uint8_t read_rx_pl_wid(void) const;
      void write_ack_payload(const uint8_t pipe, const uint8_t len, const uint8_t buf[]) const;

      void write_tx_payload_no_ack(const uint8_t len, const uint8_t buf[]);
      StatusReg nop(void) const;
    private:
      const INIT_HW::IoPin m_ceConfig = {0,0};
      chibios_rt::BinarySemaphore m_semaphore;
  };
}




#endif /* NRF24L01_HPP_ */
