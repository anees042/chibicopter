/*
    I2CDevice Class - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Revisions:
 * 	Date		Version			Author			Description
 *  24.04.2014	0.1				Rothfelder		First version based on I2Cdev lib by Jeff Rowberg
 */


/**
 * @file    I2CDevice.h
 * @brief   I2C Device Driver header.
 *
 * @addtogroup I2C
 * @{
 */

#ifndef I2CDEVICE_H
#define I2CDEVICE_H

#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "i2c.h"
#include "i2c_lld.h"

#include <stdint.h>
#include <stdbool.h>

//#define I2C_AQUIRE_BUS

class I2CDevice
{
    public:
        I2CDevice(I2CDriver *I2Cx, uint8_t address);
        void setAddress(uint8_t address);
        uint8_t getAddress();
        void timeout(uint32_t ms);
        systime_t timeout();

    private:
        uint8_t _addr;
        I2CDriver *_i2cx;
        systime_t m_timeout;
        uint32_t m_timeout_ms;
    protected:
        msg_t readByte(uint8_t memAddress, uint8_t *data, systime_t timeout);
        msg_t readBit(uint8_t memAddress, uint8_t bitNum, uint8_t *data, systime_t timeout);
        msg_t readBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data, systime_t timeout);
        msg_t readBytes(uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout);
        msg_t writeBytes(uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout);
        msg_t writeByte(uint8_t memAddress, uint8_t data, systime_t timeout);
        msg_t writeBit(uint8_t memAddress, uint8_t bitNum, uint8_t data, systime_t timeout);
        msg_t writeBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data, systime_t timeout);
        msg_t writeWord(uint8_t memAddress, uint16_t data, systime_t timeout);

        msg_t readByte(uint8_t devAddress, uint8_t memAddress, uint8_t *data, systime_t timeout);
        msg_t readBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t *data, systime_t timeout);
        msg_t readBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data, systime_t timeout);
        msg_t readBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout);
        msg_t writeBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout);
        msg_t writeByte(uint8_t devAddress, uint8_t memAddress, uint8_t data, systime_t timeout);
        msg_t writeBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t data, systime_t timeout);
        msg_t writeBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data, systime_t timeout);
        msg_t writeWord(uint8_t devAddress, uint8_t memAddress, uint16_t data, systime_t timeout);

        msg_t readByte(uint8_t devAddress, uint8_t memAddress, uint8_t *data);
        msg_t readBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t *data);
        msg_t readBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data);
        msg_t readBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data);
        msg_t writeBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data);
        msg_t writeByte(uint8_t devAddress, uint8_t memAddress, uint8_t data);
        msg_t writeBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t data);
        msg_t writeBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data);
        msg_t writeWord(uint8_t devAddress, uint8_t memAddress, uint16_t data);

        msg_t readByte(uint8_t memAddress, uint8_t *data);
        msg_t readBit(uint8_t memAddress, uint8_t bitNum, uint8_t *data);
        msg_t readBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data);
        msg_t readBytes(uint8_t memAddress, uint16_t len, uint8_t *data);
        msg_t writeBytes(uint8_t memAddress, uint16_t len, uint8_t *data);
        msg_t writeByte(uint8_t memAddress, uint8_t data);
        msg_t writeBit(uint8_t memAddress, uint8_t bitNum, uint8_t data);
        msg_t writeBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data);
        msg_t writeWord(uint8_t memAddress, uint16_t data);

};

#endif
/*! @} */
