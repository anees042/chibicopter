/*
    I2CDevice Class - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Revisions:
 * 	Date		Version			Author			Description
 *  24.04.2014	0.1				Rothfelder		Init file
 */

/*!
 * @file    init_hw.h
 * @brief   Header file for initialization functions.
 *
 * @addtogroup Init
 * @{
 */

#ifndef _INIT_HW_H_
#define _INIT_HW_H_


#include "ch.hpp"
#include "chconf.h"
#include "hal.h"
#include "chtypes.h"
#include "i2c_lld.h"
#include "i2c.h"
#include "adc.h"

namespace INIT_HW
{
  struct IoPin {
    ioportid_t port;
    uint16_t pin;
  };

  extern const ADCConversionGroup adcgrpcfg;
  void GPIO();
  void I2C();
  bool I2C_clockOutSlave(IoPin sclPin, IoPin sdaPin, bool checkSda = true);
  void SPI();
  void UART();
  void USB();
  void ADConverter();
}
#endif
/*! @} */
