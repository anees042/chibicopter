/*
 * motorcontroller.hpp
 *
 *  Created on: 09.05.2014
 *      Author: tobias
 */

#ifndef MOTORCONTROLLER_HPP_
#define MOTORCONTROLLER_HPP_

#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
//#include "uart.h"
#include "serial.h"

namespace driver {

  class MotorController
  {
    public:
      MotorController(SerialDriver *sd);
      MotorController(SerialDriver *sd, uint8_t syncByte);
      MotorController(SerialDriver *sd, uint8_t syncByte, uint8_t min, uint8_t max);
      void speed(float m1, float m2, float m3, float m4);
      void min(uint8_t newMin);
      void max(uint8_t newMax);
      uint8_t min();
      uint8_t max();
      void Test(void);
      void Lock(void);
      void Unlock(void);

    private:
      SerialDriver *m_sd;
      uint8_t m_min, m_max, m_syncByte;
      chibios_rt::BinarySemaphore m_semaphore;
  };

}



#endif /* MOTORCONTROLLER_HPP_ */
