/*
 * at25eeprom.hpp
 *
 *  Created on: 07.07.2014
 *      Author: tobias
 */

#ifndef AT25EEPROM_HPP_
#define AT25EEPROM_HPP_

#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "spi.h"
#include "spi_lld.h"

#include <stdint.h>
#include <stdbool.h>

namespace driver {
  class At25eeprom {
    public:
      struct StatusReg {
        bool nrdy;
        bool wen;
        bool bp0;
        bool bp1;
        bool dc0;
        bool dc1;
        bool dc2;
        bool wpen;
        bool writeCycle;
      };
      At25eeprom(SPIDriver *driver, SPIConfig spiConfig);
      void WriteEnabled(bool enable);
      bool WriteEnabled(void);

      StatusReg StatusRegister(void);
      void StatusRegister(StatusReg reg);

      uint8_t Read(uint16_t address);
      void Read(uint16_t address, uint16_t size, uint8_t *buf);

      void Write(uint16_t address, uint8_t byte);
      void Write(uint16_t address, uint16_t size, uint8_t *buf);

      void Lock(void);
      void Unlock(void);
    protected:

    private:
      SPIDriver *m_spiDriver;
      SPIConfig m_spiConfig;
      chibios_rt::BinarySemaphore m_semaphore;
  };
}

#endif /* AT25EEPROM_HPP_ */
