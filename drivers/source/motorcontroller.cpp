/*
 * motorcontroller.cpp
 *
 *  Created on: 09.05.2014
 *      Author: tobias
 */


#include "motorcontroller.hpp"
#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
//#include "uart.h"
#include "serial.h"
#include "chprintf.h"

namespace driver {

  MotorController::MotorController(SerialDriver *sd) : m_sd(sd), m_min(25), m_max(200), m_syncByte(0xF5), m_semaphore(false){
  }

  MotorController::MotorController(SerialDriver *sd, uint8_t syncByte) : m_sd(sd), m_min(25), m_max(200), m_syncByte(syncByte), m_semaphore(false) {
  }

  MotorController::MotorController(SerialDriver *sd, uint8_t syncByte, uint8_t min, uint8_t max) : m_sd(sd), m_min(min), m_max(max), m_syncByte(syncByte), m_semaphore(false) {
  }

  void MotorController::speed(float m1, float m2, float m3, float m4)
  {
    uint8_t motors[4];

    if (m1 > this->max())
      m1 = this->max();
    if (m2 > this->max())
      m2 = this->max();
    if (m3 > this->max())
      m3 = this->max();
    if (m4 > this->max())
      m4 = this->max();

    if (m1 < this->min())
      m1 = this->min();
    if (m2 < this->min())
      m2 = this->min();
    if (m3 < this->min())
      m3 = this->min();
    if (m4 < this->min())
      m4 = this->min();

    motors[0] = static_cast<uint8_t>(m1);
    motors[1] = static_cast<uint8_t>(m2);
    motors[2] = static_cast<uint8_t>(m3);
    motors[3] = static_cast<uint8_t>(m4);

    sdPut(m_sd, m_syncByte);
    for (int i = 0; i<4; i++)
      sdPut(m_sd, motors[i]);
    //uartStartSend(m_uart,5,motors);
    //sdWrite(m_sd, motors, 5);
    //chprintf(reinterpret_cast<BaseSequentialStream *>(m_sd), reinterpret_cast<const char *>(motors));
    //chSequentialStreamWrite(reinterpret_cast<BaseSequentialStream *>(m_sd), motors, 5);
  }

  void MotorController::min(uint8_t newMin)
  {
    m_min = newMin;
  }

  void MotorController::max(uint8_t newMax)
  {
    m_max = newMax;
  }

  uint8_t MotorController::min()
  {
    return m_min;
  }

  uint8_t MotorController::max()
  {
    return m_max;
  }

  void MotorController::Test(void)
  {
    sdPut(m_sd, m_syncByte);
    sdPut(m_sd, m_min);
    sdPut(m_sd, 0);
    sdPut(m_sd, 0);
    sdPut(m_sd, 0);

    chibios_rt::BaseThread::sleep(MS2ST(500));
    sdPut(m_sd, m_syncByte);
    sdPut(m_sd, 0);
    sdPut(m_sd, m_min);
    sdPut(m_sd, 0);
    sdPut(m_sd, 0);

    chibios_rt::BaseThread::sleep(MS2ST(500));
    sdPut(m_sd, m_syncByte);
    sdPut(m_sd, 0);
    sdPut(m_sd, 0);
    sdPut(m_sd, m_min);
    sdPut(m_sd, 0);

    chibios_rt::BaseThread::sleep(MS2ST(500));
    sdPut(m_sd, m_syncByte);
    sdPut(m_sd, 0);
    sdPut(m_sd, 0);
    sdPut(m_sd, 0);
    sdPut(m_sd, m_min);
  }

  void MotorController::Lock(void)
  {
    this->m_semaphore.wait();
  }

  void MotorController::Unlock(void)
  {
    this->m_semaphore.signal();
  }
}
