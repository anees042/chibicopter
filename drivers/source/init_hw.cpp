/*
    I2CDevice Class - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Revisions:
 * 	Date		Version			Author			Description
 *  24.04.2014	0.1				Rothfelder		Init file
 */

/*!
 * @file    init_hw.cpp
 * @brief   Source file for initialization functions.
 *
 * @addtogroup Init
 * @{
 */

#include "init_hw.h"
#include "sensors.hpp"


#include "ch.hpp"
#include "stm32f4xx.h"
#include "chconf.h"
#include "hal.h"
#include "pal.h"
#include "chtypes.h"
#include "i2c_lld.h"
#include "i2c.h"
#include "adc.h"

extern "C" {
  #include "myUSB.h"
}

namespace INIT_HW {
  /*
   * ADC conversion group.
   * Mode:        Linear buffer, 3 channels, SW triggered.
   * Channels:    IN11   (480 cycles sample time)
   *              Sensor (192 cycles sample time)
   */
  const ADCConversionGroup adcgrpcfg = {
    FALSE,
    3,
    NULL,
    NULL,
    /* HW dependent part.*/
    0,                        /* CR1 */
    ADC_CR2_SWSTART,          /* CR2 */
    ADC_SMPR1_SMP_AN11(ADC_SAMPLE_480) | ADC_SMPR1_SMP_AN14(ADC_SAMPLE_480) | ADC_SMPR1_SMP_AN15(ADC_SAMPLE_480),
    0,                        /* SMPR2 */
    ADC_SQR1_NUM_CH(3),
    0,
    ADC_SQR3_SQ1_N(ADC_CHANNEL_IN11) | ADC_SQR3_SQ2_N(ADC_CHANNEL_IN14) | ADC_SQR3_SQ3_N(ADC_CHANNEL_IN15)
  };

    void GPIO()
    {
      palSetPadMode(GPIOD,  0, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* Navi Leds */
      palSetPadMode(GPIOD, 12, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* Green LED */
      palSetPadMode(GPIOD, 13, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* Orange LED */
      palSetPadMode(GPIOD, 14, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* Red LED */
      palSetPadMode(GPIOD, 15, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* Blue LED */
      palSetPadMode(GPIOD,  1, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* Buzzer */
      palSetPadMode(GPIOB,  4, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* RF - LED Yellow */
      palSetPadMode(GPIOA,  8, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);   /* RF - LED Green*/
      palClearPad(GPIOD, 1);
      palClearPad(GPIOD, 0);
      palClearPad(GPIOB, 4);
      palClearPad(GPIOA, 8);


      palSetPadMode(GPIOA, 0, PAL_MODE_INPUT_PULLDOWN); /* Interrupt from Button*/
      palSetPadMode(GPIOB, 1, PAL_MODE_INPUT_PULLDOWN); /* Interrupt from NRF24L01+*/
      palSetPadMode(GPIOB, 7, PAL_MODE_INPUT_PULLDOWN); /* Interrupt from MPU6050/9150*/


      palSetPadMode(GPIOC, 1, PAL_MODE_INPUT_ANALOG); /* ADC1 Ch 11 Akku*/
      palSetPadMode(GPIOC, 4, PAL_MODE_INPUT_ANALOG); /* ADC1 Ch 14 Akku*/
      palSetPadMode(GPIOC, 5, PAL_MODE_INPUT_ANALOG); /* ADC1 Ch 15 Akku*/

    }

	void I2C()
	{
		static const I2CConfig i2cfg1 = {
			OPMODE_I2C,
			400000,
			FAST_DUTY_CYCLE_2, //STD_DUTY_CYCLE FAST_DUTY_CYCLE_16_9 FAST_DUTY_CYCLE_2,
		};

		IoPin sda, scl;
		sda.port = GPIOB;
		sda.pin = 9;
		scl.port = GPIOB;
		scl.pin = 8;

		// Link PB8 and PB9 to I2C1 function
		if ( I2C_clockOutSlave(scl, sda) )
		{
          //palSetPadMode(GPIOB, 8, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);// | PAL_MODE_OUTPUT_PUSHPULL); // | PAL_STM32_OSPEED_HIGHEST);   /* SCL */
          //palSetPadMode(GPIOB, 9, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);// | PAL_MODE_OUTPUT_PUSHPULL); //| PAL_STM32_OSPEED_HIGHEST);   /* SDA */
          i2cStart(&I2CD1, &i2cfg1);
		}
	}

	bool I2C_clockOutSlave(IoPin sclPin, IoPin sdaPin, bool checkSda)
	{
	  const uint8_t clkCount = 8;
	  uint8_t tmp = 0;
	  bool sdaPinHigh;

      palSetPadMode(sclPin.port, sclPin.pin, PAL_MODE_OUTPUT_PUSHPULL | PAL_STM32_OSPEED_HIGHEST);// | PAL_MODE_OUTPUT_PUSHPULL); // | PAL_STM32_OSPEED_HIGHEST);   /* SCL */
      palSetPadMode(sdaPin.port, sdaPin.pin, PAL_MODE_INPUT | PAL_STM32_OSPEED_HIGHEST);// | PAL_MODE_OUTPUT_PUSHPULL); //| PAL_STM32_OSPEED_HIGHEST);   /* SDA */

      palSetPad(sclPin.port, sclPin.pin);
      sdaPinHigh = ( palReadPad(sdaPin.port,sdaPin.pin) == PAL_HIGH );
      while ( (!checkSda or !sdaPinHigh) and (tmp < clkCount) )
      {
        palClearPad(sclPin.port, sclPin.pin);
        chibios_rt::BaseThread::sleep(US2ST(10));
        palSetPad(sclPin.port, sclPin.pin);
        chibios_rt::BaseThread::sleep(US2ST(10));
        sdaPinHigh = (palReadPad(sdaPin.port,sdaPin.pin) == PAL_HIGH);
        tmp += 1;
      }

      palSetPadMode(sclPin.port, sclPin.pin, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);// | PAL_MODE_OUTPUT_PUSHPULL); // | PAL_STM32_OSPEED_HIGHEST);   /* SCL */
      palSetPadMode(sdaPin.port, sdaPin.pin, PAL_MODE_ALTERNATE(4) | PAL_STM32_OTYPE_OPENDRAIN);// | PAL_MODE_OUTPUT_PUSHPULL); //| PAL_STM32_OSPEED_HIGHEST);   /* SDA */
	  return sdaPinHigh;
	}

	void SPI()
	{
	  /*
	   * SPI2 I/O pins setup.
	   */
	  palSetPadMode(GPIOB, 13, PAL_MODE_ALTERNATE(5) |
	                           PAL_STM32_OSPEED_HIGHEST);       /* New SCK.     */
	  palSetPadMode(GPIOB, 14, PAL_MODE_ALTERNATE(5) |
	                           PAL_STM32_OSPEED_HIGHEST);       /* New MISO.    */
	  palSetPadMode(GPIOB, 15, PAL_MODE_ALTERNATE(5) |
	                           PAL_STM32_OSPEED_HIGHEST);       /* New MOSI.    */
	  palSetPadMode(GPIOB,  2, PAL_MODE_OUTPUT_PUSHPULL |
	                           PAL_STM32_OSPEED_HIGHEST);       /* New CS.      */
	  palSetPadMode(GPIOA, 15, PAL_MODE_OUTPUT_PUSHPULL |
	                           PAL_STM32_OSPEED_HIGHEST);

	  palSetPad(GPIOA,15);
	  palSetPad(GPIOB, 2);
	}

    void UART()
    {

        //static UARTConfig motorUartcfg = {
        //   NULL,                    /* End of Transmission buffer callback               */
        //   NULL,                    /* Physical end of transmission callback             */
        //   NULL,                    /* Receive buffer filled callback                    */
        //   NULL,                   /* Char received while out of the UART_RECEIVE state */
        //   NULL,                    /* Receive error callback                            */
        //   115200,                     /* Baudrate                                          */
        //   0,                       /* cr1 register values                               */
        //   0,                       /* cr2 register values                               */
        //   0                        /* cr3 register values                               */
        //};
        static SerialConfig motorUartcfg = {
           115200,                     /* Baudrate                                          */
           0,                       /* cr1 register values                               */
           0,                       /* cr2 register values                               */
           0                        /* cr3 register values                               */
        };

        static SerialConfig debugSDcfg = {
           115200,                     /* Baudrate                                          */
           0,                       /* cr1 register values                               */
           0,                       /* cr2 register values                               */
           0                        /* cr3 register values                               */
        };



        // Link PB6 UART1 TX
        palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(7));   /* TX */
        // Link PD8 and PD9 UART3 TX and RX
        palSetPadMode(GPIOD, 8, PAL_MODE_ALTERNATE(7));   /* TX */
        palSetPadMode(GPIOD, 9, PAL_MODE_ALTERNATE(7));   /* RX */

        //uartStart(&UARTD1, &motorUartcfg);
        sdStart(&SD1, &motorUartcfg);
        sdStart(&SD3, &debugSDcfg);
    }

    void USB()
    {
      myUSBinit();
    }

    void ADConverter()
    {
      adcStart(&ADCD1, NULL);
    }

}
/*! @} */
