/*
 * SPIDevice.cpp
 *
 *  Created on: 25.06.2014
 *      Author: tobias
 */


#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "spi.h"
#include "spi_lld.h"

#include <stdint.h>
#include <stdbool.h>
#include "SPIDevice.hpp"

namespace modules {

  SPIDevice::SPIDevice(SPIDriver *driver, SPIConfig cfg) : m_driver(driver), m_spiConfig(cfg)
  {
  }


}
