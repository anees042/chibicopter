/*
 * nrf24l01.cpp
 *
 *  Created on: 18.06.2014
 *      Author: tobias
 */

#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "spi.h"
#include "spi_lld.h"

#include <stdint.h>
#include <stdbool.h>
#include "SPIDevice.hpp"
#include "nrf24l01.hpp"
#include "nrf24l01mnemonics.h"
#include "init_hw.h"

namespace modules {

  Nrf24l01p::Nrf24l01p(SPIDriver *driver, SPIConfig spiConfig, INIT_HW::IoPin ce) : m_driver(driver), m_spiConfig(spiConfig), m_semaphore(true)//SPIDevice(driver, spiConfig), m_semaphore(true)
  {
    INIT_HW::IoPin *tmpIo = const_cast<INIT_HW::IoPin *>(&this->m_ceConfig);
    *tmpIo = ce;

    palSetPadMode(this->m_ceConfig.port, this->m_ceConfig.pin, PAL_MODE_OUTPUT_PUSHPULL);
    EnterStandby();
    Unlock();
  }

  void Nrf24l01p::Lock(void)
  {
    this->m_semaphore.wait();
  }

  void Nrf24l01p::Unlock(void)
  {
    this->m_semaphore.signal();
  }

  void Nrf24l01p::chipEnable(bool enable) const
  {
    if (enable)
      palSetPad(this->m_ceConfig.port, this->m_ceConfig.pin); //ceauf high -> chip active
    else
      palClearPad(this->m_ceConfig.port, this->m_ceConfig.pin); //ceauf low -> chip inactive
  }

  bool Nrf24l01p::chipEnable(void) const
  {
    return (palReadPad(this->m_ceConfig.port, this->m_ceConfig.pin) == PAL_HIGH);
  }

  void Nrf24l01p::read_register(const uint8_t reg, const uint8_t len, uint8_t buf[]) const
  {
    uint8_t op = R_REGISTER | (REGISTER_MASK & reg);
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiReceive(this->m_driver, len, buf);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */

  }

  void Nrf24l01p::write_register(const uint8_t reg, const uint8_t len, const uint8_t buf[]) const
  {
    uint8_t op = W_REGISTER | (REGISTER_MASK & reg);
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiSend(this->m_driver, len, buf);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  void Nrf24l01p::read_rx_payload(const uint8_t len, uint8_t buf[]) const
  {
    uint8_t op = R_RX_PAYLOAD;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiReceive(this->m_driver, len, buf);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  void Nrf24l01p::write_tx_payload(const uint8_t len, const uint8_t buf[]) const
  {
    uint8_t op = W_TX_PAYLOAD;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiSend(this->m_driver, len, buf);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  void Nrf24l01p::flush_tx(void) const
  {
    uint8_t op = FLUSH_TX;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  void Nrf24l01p::flush_rx(void) const
  {
    uint8_t op = FLUSH_RX;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  void Nrf24l01p::reuse_tx_pl(void) const
  {
    uint8_t op = REUSE_TX_PL;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  uint8_t Nrf24l01p::read_rx_pl_wid(void) const
  {
    uint8_t op = R_RX_PL_WID;
    uint8_t ret = 0;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiReceive(this->m_driver, 1, &ret);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */

    return ret;
  }

  void Nrf24l01p::write_ack_payload(const uint8_t pipe, const uint8_t len, const uint8_t buf[]) const
  {
    uint8_t op = W_ACK_PAYLOAD | (0b111 & pipe);
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiSend(this->m_driver, len, buf);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  void Nrf24l01p::write_tx_payload_no_ack(const uint8_t len, const uint8_t buf[])
  {
    uint8_t op = W_TX_PAYLOAD_NO_ACK;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiSend(this->m_driver, len, buf);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
  }

  Nrf24l01p::StatusReg Nrf24l01p::nop(void) const
  {
    uint8_t op = NOP;
    StatusReg ret;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_driver); /* Acquire ownership of the bus. */
    spiStart(this->m_driver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_driver); /* Slave Select assertion. */
    spiSend(this->m_driver, 1, &op); /* Atomic transfer operations. */
    spiSend(this->m_driver, 1, &ret.reg);
    spiUnselect(this->m_driver); /* Slave Select de-assertion. */
    spiStop(this->m_driver);
    spiReleaseBus(this->m_driver); /* Ownership release. */
    return ret;
  }

  void Nrf24l01p::EnterRx(void)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);
    data |= 1<<PRIM_RX;
    this->write_register(CONFIG, 1, &data);
  }

  void Nrf24l01p::EnterTx(void)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);
    data &= ~(1<<PRIM_RX);
    this->write_register(CONFIG, 1, &data);
  }

  void Nrf24l01p::PowerUp(void)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);
    data |= (1<<PWR_UP);
    this->write_register(CONFIG, 1, &data);
    this->chipEnable(true);
  }

  void Nrf24l01p::PowerDown(void)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);
    data &= ~(1<<PWR_UP);
    this->write_register(CONFIG, 1, &data);
    this->chipEnable(true);
  }

  void Nrf24l01p::EnterStandby(void)
  {
    /*
    uint8_t data;
    this->read_register(CONFIG, 1, &data);
    data &= ~(1<<PWR_UP);
    this->write_register(CONFIG, 1, &data);
    */
    this->chipEnable(false);
  }

  void Nrf24l01p::Crc(Nrf24l01p::CrcLength newCrc)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);
    switch (newCrc)
    {
      case Nrf24l01p::CrcLength::RF24_CRC_DISABLED:
                                                    data &= ~((1<<EN_CRC) | (1<<CRCO));
                                                    break;
      case Nrf24l01p::CrcLength::RF24_CRC_8:
                                                    data |= (1<<EN_CRC);
                                                    data &= ~(1<<CRCO);
                                                    break;
      case Nrf24l01p::CrcLength::RF24_CRC_16:
                                                    data |= (1<<EN_CRC) | (1<<CRCO);
                                                    break;
    }
    this->write_register(CONFIG, 1, &data);
  }

  void Nrf24l01p::InterruptMaxRetries(bool enabled)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);

    if (enabled)
      data |= (1<<MASK_MAX_RT);
    else
      data &= ~(1<<MASK_MAX_RT);

    this->write_register(CONFIG, 1, &data);
  }

  void Nrf24l01p::InterruptTxDataSent(bool enabled)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);

    if (enabled)
      data |= (1<<MASK_TX_DS);
    else
      data &= ~(1<<MASK_TX_DS);

    this->write_register(CONFIG, 1, &data);
  }

  void Nrf24l01p::InterruptRxDataReceived(bool enabled)
  {
    uint8_t data;
    this->read_register(CONFIG, 1, &data);

    if (enabled)
      data |= (1<<MASK_RX_DR);
    else
      data &= ~(1<<MASK_RX_DR);

    this->write_register(CONFIG, 1, &data);
  }

  void Nrf24l01p::ClearInterruptMaxRetriesFlag(void)
  {
    Nrf24l01p::StatusReg reg;
    reg.reg = 0;
    reg.max_rt = 1;
    this->write_register(STATUS, 1, &reg.reg);
  }

  void Nrf24l01p::ClearInterruptTxDataSentFlag(void)
  {
    Nrf24l01p::StatusReg reg;
    reg.reg = 0;
    reg.tx_ds = 1;
    this->write_register(STATUS, 1, &reg.reg);
  }

  void Nrf24l01p::ClearInterruptRxDataReceivedFlag(void)
  {
    Nrf24l01p::StatusReg reg;
    reg.reg = 0;
    reg.rx_dr = 1;
    this->write_register(STATUS, 1, &reg.reg);
  }

  void Nrf24l01p::ClearInterruptAllFlags(void)
  {
    Nrf24l01p::StatusReg reg;
    reg.reg = 0;
    reg.max_rt = 1;
    reg.tx_ds = 1;
    reg.rx_dr = 1;
    this->write_register(STATUS, 1, &reg.reg);
  }

  void Nrf24l01p::EnableAutoAcknowledgment(uint8_t pipeMask, bool enabled)
  {
    uint8_t data;
    this->read_register(EN_AA, 1, &data);

    if (enabled)
      data |= (pipeMask & 0b00111111);
    else
      data &= ~(pipeMask | 0b11000000);

    this->write_register(EN_AA, 1, &data);
  }

  void Nrf24l01p::EnableRxAddresses(uint8_t addressMask, bool enabled)
  {
    uint8_t data;
    this->read_register(EN_RXADDR, 1, &data);

    if (enabled)
      data |= (addressMask & 0b00111111);
    else
      data &= ~(addressMask | 0b11000000);

    this->write_register(EN_RXADDR, 1, &data);
  }

  void Nrf24l01p::SetupAddressWidth(AddressWidth newWidth)
  {
    uint8_t data;

    switch (newWidth)
    {
      case Nrf24l01p::AddressWidth::RF24_3BYTES:  data = 0b01; break;
      case Nrf24l01p::AddressWidth::RF24_4BYTES:  data = 0b10; break;
      case Nrf24l01p::AddressWidth::RF24_5BYTES:  data = 0b11; break;
    }

    this->write_register(SETUP_AW, 1, &data);
  }

  void Nrf24l01p::AutomaticRetransmissionDelay(uint8_t delayMask)
  {
    uint8_t data;
    this->read_register(SETUP_RETR, 1, &data);
    data &= ~(0b11110000);
    data |= 0b11110000 & delayMask;
    this->write_register(SETUP_RETR, 1, &data);
  }

  void Nrf24l01p::AutomaticRetransmissionCount(uint8_t countMask)
  {
    uint8_t data;
    this->read_register(SETUP_RETR, 1, &data);
    data &= ~(0b1111);
    data |= 0b1111 & countMask;
    this->write_register(SETUP_RETR, 1, &data);
  }

  void Nrf24l01p::RfChannel(uint8_t newChannel)
  {
    newChannel &= ~(0b10000000);
    this->write_register(RF_CH, 1, &newChannel);
  }

  void Nrf24l01p::EnableContinuousCarrier(bool enabled)
  {
    uint8_t data;
    this->read_register(RF_SETUP, 1, &data);

    if (enabled)
      data |= (1<<CONT_WAVE);
    else
      data &= ~(1<<CONT_WAVE);

    this->write_register(RF_SETUP, 1, &data);
  }

  void Nrf24l01p::SetDataRate(DataRate rate)
  {
    uint8_t data;
    this->read_register(RF_SETUP, 1, &data);

    data &= ~((1<<RF_DR_LOW) | (1<<RF_DR_HIGH));
    switch (rate)
    {
      case Nrf24l01p::DataRate::RF24_250KBPS: data |= (1<<RF_DR_LOW); break;
      case Nrf24l01p::DataRate::RF24_1MBPS: break;
      case Nrf24l01p::DataRate::RF24_2MBPS: data |= (1<<RF_DR_HIGH); break;
    }

    this->write_register(RF_SETUP, 1, &data);
  }

  void Nrf24l01p::ForcePllLock(bool enabled)
  {
    uint8_t data;
    this->read_register(RF_SETUP, 1, &data);

    if (enabled)
      data |= (1<<PLL_LOCK);
    else
      data &= ~(1<<PLL_LOCK);

    this->write_register(RF_SETUP, 1, &data);
  }

  void Nrf24l01p::RfPower(Pa_dbm newRfPower)
  {
    uint8_t data;
    this->read_register(RF_SETUP, 1, &data);

    data &= ~(0b00000110);

    switch (newRfPower)
    {
      case Nrf24l01p::Pa_dbm::RF24_PA_ERROR : break;
      case Nrf24l01p::Pa_dbm::RF24_PA_MIN :  data |= 0b00; break;
      case Nrf24l01p::Pa_dbm::RF24_PA_LOW :  data |= 0b01; break;
      case Nrf24l01p::Pa_dbm::RF24_PA_HIGH : data |= 0b10; break;
      case Nrf24l01p::Pa_dbm::RF24_PA_MAX :  data |= 0b11; break;
    }

    this->write_register(RF_SETUP, 1, &data);
  }

  Nrf24l01p::StatusReg Nrf24l01p::GetStatus(void)
  {
    return this->nop();
  }

  uint8_t Nrf24l01p::LostPackets(void)
  {
    uint8_t data = 0;
    this->read_register(OBSERVE_TX, 1, &data);
    data = data>>4;
    return data;
  }

  uint8_t Nrf24l01p::RetransmittedCount(void)
  {
    uint8_t data = 0;
    this->read_register(OBSERVE_TX, 1, &data);
    data &= 0b00001111;
    return data;
  }

  bool Nrf24l01p::ReceivedPowerDetector(void)
  {
    uint8_t data;

    this->read_register(RPD, 1, &data);

    return data;
  }

  void Nrf24l01p::RxAddressPipe0(const uint8_t address[5])
  {
    this->write_register(RX_ADDR_P0, 5, address);
  }

  void Nrf24l01p::RxAddressPipe1(const uint8_t address[5])
  {
    this->write_register(RX_ADDR_P1, 5, address);
  }

  void Nrf24l01p::RxAddressPipe2(const uint8_t address)
  {
    this->write_register(RX_ADDR_P2, 1, &address);
  }

  void Nrf24l01p::RxAddressPipe3(const uint8_t address)
  {
    this->write_register(RX_ADDR_P3, 1, &address);
  }

  void Nrf24l01p::RxAddressPipe4(const uint8_t address)
  {
    this->write_register(RX_ADDR_P4, 1, &address);
  }

  void Nrf24l01p::RxAddressPipe5(const uint8_t address)
  {
    this->write_register(RX_ADDR_P5, 1, &address);
  }

  void Nrf24l01p::TxAddress(const uint8_t address[5])
  {
    this->write_register(TX_ADDR, 5, address);
  }

  void Nrf24l01p::RxPayloadWidthPipe0(uint8_t numBytes)
  {
    numBytes &= 0b00111111;
    this->write_register(RX_PW_P0, 1, &numBytes);
  }

  void Nrf24l01p::RxPayloadWidthPipe1(uint8_t numBytes)
  {
    numBytes &= 0b00111111;
    this->write_register(RX_PW_P1, 1, &numBytes);
  }

  void Nrf24l01p::RxPayloadWidthPipe2(uint8_t numBytes)
  {
    numBytes &= 0b00111111;
    this->write_register(RX_PW_P2, 1, &numBytes);
  }

  void Nrf24l01p::RxPayloadWidthPipe3(uint8_t numBytes)
  {
    numBytes &= 0b00111111;
    this->write_register(RX_PW_P3, 1, &numBytes);
  }

  void Nrf24l01p::RxPayloadWidthPipe4(uint8_t numBytes)
  {
    numBytes &= 0b00111111;
    this->write_register(RX_PW_P4, 1, &numBytes);
  }

  void Nrf24l01p::RxPayloadWidthPipe5(uint8_t numBytes)
  {
    numBytes &= 0b00111111;
    this->write_register(RX_PW_P5, 1, &numBytes);
  }

  bool Nrf24l01p::FifoTxReuse(void)
  {
    uint8_t data;
    this->read_register(FIFO_STATUS, 1, &data);
    return data>>6 & 1;
  }

  bool Nrf24l01p::FifoTxFull(void)
  {
    uint8_t data;
    this->read_register(FIFO_STATUS, 1, &data);
    return data>>5 & 1;
  }

  bool Nrf24l01p::FifoTxEmpty(void)
  {
    uint8_t data;
    this->read_register(FIFO_STATUS, 1, &data);
    return data>>4 & 1;
  }

  bool Nrf24l01p::FifoRxFull(void)
  {
    uint8_t data;
    this->read_register(FIFO_STATUS, 1, &data);
    return data>>1 & 1;
  }

  bool Nrf24l01p::FifoRxEmpty(void)
  {
    uint8_t data;
    this->read_register(FIFO_STATUS, 1, &data);
    return data>>0 & 1;
  }

  void Nrf24l01p::EnableDynamicPayloadPipe0(bool enabled)
  {
    uint8_t data;
    this->read_register(DYNPD, 1, &data);

    if (enabled)
      data |= 1<<0;
    else
      data &= ~(1<<0);

    this->write_register(DYNPD, 1, &data);
  }

  void Nrf24l01p::EnableDynamicPayloadPipe1(bool enabled)
  {
    uint8_t data;
    this->read_register(DYNPD, 1, &data);

    if (enabled)
      data |= 1<<1;
    else
      data &= ~(1<<1);

    this->write_register(DYNPD, 1, &data);
  }

  void Nrf24l01p::EnableDynamicPayloadPipe2(bool enabled)
  {
    uint8_t data;
    this->read_register(DYNPD, 1, &data);

    if (enabled)
      data |= 1<<2;
    else
      data &= ~(1<<2);

    this->write_register(DYNPD, 1, &data);
  }

  void Nrf24l01p::EnableDynamicPayloadPipe3(bool enabled)
  {
    uint8_t data;
    this->read_register(DYNPD, 1, &data);

    if (enabled)
      data |= 1<<3;
    else
      data &= ~(1<<3);

    this->write_register(DYNPD, 1, &data);
  }

  void Nrf24l01p::EnableDynamicPayloadPipe4(bool enabled)
  {
    uint8_t data;
    this->read_register(DYNPD, 1, &data);

    if (enabled)
      data |= 1<<4;
    else
      data &= ~(1<<4);

    this->write_register(DYNPD, 1, &data);
  }

  void Nrf24l01p::EnableDynamicPayloadPipe5(bool enabled)
  {
    uint8_t data;
    this->read_register(DYNPD, 1, &data);

    if (enabled)
      data |= 1<<5;
    else
      data &= ~(1<<5);

    this->write_register(DYNPD, 1, &data);
  }

  void Nrf24l01p::EnableDynamicPayloadLength(bool enabled)
  {
    uint8_t data;
    this->read_register(FEATURE, 1, &data);

    if (enabled)
      data |= 1<<2;
    else
      data &= ~(1<<2);

    this->write_register(FEATURE, 1, &data);
  }

  void Nrf24l01p::EnablePayloadWithAck(bool enabled)
  {
    uint8_t data;
    this->read_register(FEATURE, 1, &data);

    if (enabled)
      data |= 1<<1;
    else
      data &= ~(1<<1);

    this->write_register(FEATURE, 1, &data);
  }

  void Nrf24l01p::EnableDynamicAck(bool enabled)
  {
    uint8_t data;
    this->read_register(FEATURE, 1, &data);

    if (enabled)
      data |= 1<<0;
    else
      data &= ~(1<<0);

    this->write_register(FEATURE, 1, &data);
  }


}

