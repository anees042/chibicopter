/*
 * at25eeprom.cpp
 *
 *  Created on: 07.07.2014
 *      Author: tobias
 */

#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "spi.h"
#include "spi_lld.h"

#include <stdint.h>
#include <stdbool.h>
#include "at25eeprom.hpp"

#define WREN  0b00000110u
#define WRDI  0b00000100u
#define RDSR  0b00000101u
#define WRSR  0b00000001u
#define READ  0b00000011u
#define WRITE 0b00000010u

namespace driver {

  At25eeprom::At25eeprom(SPIDriver *driver, SPIConfig spiConfig) : m_spiDriver(driver), m_spiConfig(spiConfig), m_semaphore(false)
  {
  }

  void At25eeprom::WriteEnabled(bool enable)
  {
    uint8_t command = (enable ? (WREN) : (WRDI));
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_spiDriver); /* Acquire ownership of the bus. */
    spiStart(this->m_spiDriver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_spiDriver); /* Slave Select assertion. */
    spiSend(this->m_spiDriver, static_cast<size_t>(1u), &command); /* Atomic transfer operations. */
    spiUnselect(this->m_spiDriver); /* Slave Select de-assertion. */
    spiStop(this->m_spiDriver);
    spiReleaseBus(this->m_spiDriver); /* Ownership release. */
  }

  bool At25eeprom::WriteEnabled(void)
  {
    return this->StatusRegister().wen;
  }

  At25eeprom::StatusReg At25eeprom::StatusRegister(void)
  {
    At25eeprom::StatusReg reg;
    uint8_t tmpTx, tmpRx;

    tmpTx = (RDSR);
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_spiDriver); /* Acquire ownership of the bus. */
    spiStart(this->m_spiDriver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_spiDriver); /* Slave Select assertion. */
    spiSend(this->m_spiDriver, static_cast<size_t>(1u), &tmpTx); /* Atomic transfer operations. */
    spiReceive(this->m_spiDriver, static_cast<size_t>(1u), &tmpRx); /* Atomic receive operations. */
    spiUnselect(this->m_spiDriver); /* Slave Select de-assertion. */
    spiStop(this->m_spiDriver);
    spiReleaseBus(this->m_spiDriver); /* Ownership release. */


    reg.nrdy = (tmpRx>>0) & 1;
    reg.wen =  (tmpRx>>1) & 1;
    reg.bp0 =  (tmpRx>>2) & 1;
    reg.bp1 =  (tmpRx>>3) & 1;
    reg.dc0 =  (tmpRx>>4) & 1;
    reg.dc1 =  (tmpRx>>5) & 1;
    reg.dc2 =  (tmpRx>>6) & 1;
    reg.wpen = (tmpRx>>7) & 1;

    return reg;
  }

  void At25eeprom::StatusRegister(At25eeprom::StatusReg reg)
  {
    uint8_t regTmp;
    regTmp  = (reg.bp0  ? 1<<2 : 0);
    regTmp |= (reg.bp1  ? 1<<3 : 0);
    regTmp |= (reg.wpen ? 1<<7 : 0);

    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_spiDriver); /* Acquire ownership of the bus. */
    spiStart(this->m_spiDriver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_spiDriver); /* Slave Select assertion. */
    spiSend(this->m_spiDriver, static_cast<size_t>(1u), &regTmp); /* Atomic transfer operations. */
    spiUnselect(this->m_spiDriver); /* Slave Select de-assertion. */
    spiStop(this->m_spiDriver);
    spiReleaseBus(this->m_spiDriver); /* Ownership release. */

  }

  uint8_t At25eeprom::Read(uint16_t address)
  {
    uint8_t tmpRx = 0;
    uint8_t tmp[3]; //could be optimized since memory map is little endian -> less readable
    tmp[0] = (READ);
    tmp[1] = static_cast<uint8_t>( (address>>8) & 0xFF ); //MSB
    tmp[2] = static_cast<uint8_t>(  address     & 0xFF ); //LSB
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_spiDriver); /* Acquire ownership of the bus. */
    spiStart(this->m_spiDriver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_spiDriver); /* Slave Select assertion. */
    spiSend(this->m_spiDriver, static_cast<size_t>(3u), tmp);
    spiReceive(this->m_spiDriver, static_cast<size_t>(1u), &tmpRx);
    spiUnselect(this->m_spiDriver); /* Slave Select de-assertion. */
    spiStop(this->m_spiDriver);
    spiReleaseBus(this->m_spiDriver); /* Ownership release. */

    return tmpRx;
  }

  void At25eeprom::Read(uint16_t address, uint16_t size, uint8_t *buf)
  {
    uint8_t tmp[3]; //could be optimized since memory map is little endian -> less readable
    tmp[0] = (READ);
    tmp[1] = static_cast<uint8_t>( (address>>8) & 0xFF ); //MSB
    tmp[2] = static_cast<uint8_t>(  address     & 0xFF ); //LSB
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_spiDriver); /* Acquire ownership of the bus. */
    spiStart(this->m_spiDriver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_spiDriver); /* Slave Select assertion. */
    spiSend(this->m_spiDriver, static_cast<size_t>(3u), tmp);
    spiReceive(this->m_spiDriver, static_cast<size_t>(size), buf);
    spiUnselect(this->m_spiDriver); /* Slave Select de-assertion. */
    spiStop(this->m_spiDriver);
    spiReleaseBus(this->m_spiDriver); /* Ownership release. */
  }

  void At25eeprom::Write(uint16_t address, uint8_t byte)
  {
    uint8_t tmp[4]; //could be optimized since memory map is little endian -> less readable
    tmp[0] = (WRITE);
    tmp[1] = static_cast<uint8_t>( (address>>8) & 0xFF ); //MSB
    tmp[2] = static_cast<uint8_t>(  address     & 0xFF ); //LSB
    tmp[3] = byte;
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_spiDriver); /* Acquire ownership of the bus. */
    spiStart(this->m_spiDriver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_spiDriver); /* Slave Select assertion. */
    spiSend(this->m_spiDriver, static_cast<size_t>(4u), tmp);
    //spiSend(this->m_spiDriver, static_cast<size_t>(1u), &byte); /* Atomic transfer operations. */
    spiUnselect(this->m_spiDriver); /* Slave Select de-assertion. */
    spiStop(this->m_spiDriver);
    spiReleaseBus(this->m_spiDriver); /* Ownership release. */
  }

  void At25eeprom::Write(uint16_t address, uint16_t size, uint8_t *buf)
  {
    uint8_t tmp[3]; //could be optimized since memory map is little endian -> less readable
    tmp[0] = (WRITE);
    tmp[1] = static_cast<uint8_t>( (address>>8) & 0xFF ); //MSB
    tmp[2] = static_cast<uint8_t>(  address     & 0xFF ); //LSB
    /*
    * Do exchange between device and MCU.
    */
    spiAcquireBus(this->m_spiDriver); /* Acquire ownership of the bus. */
    spiStart(this->m_spiDriver, &this->m_spiConfig); /* Setup transfer parameters. */
    spiSelect(this->m_spiDriver); /* Slave Select assertion. */
    spiSend(this->m_spiDriver, static_cast<size_t>(3u), tmp);
    spiSend(this->m_spiDriver, static_cast<size_t>(size), buf); /* Atomic transfer operations. */
    spiUnselect(this->m_spiDriver); /* Slave Select de-assertion. */
    spiStop(this->m_spiDriver);
    spiReleaseBus(this->m_spiDriver); /* Ownership release. */
  }

}
