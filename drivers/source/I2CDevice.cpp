/*
    I2CDevice Class - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Revisions:
 * 	Date		Version			Author			Description
 *  24.04.2014	0.1				Rothfelder		First version based on I2Cdev lib by Jeff Rowberg
 */


/**
 * @file    I2CDevice.cpp
 * @brief   I2C Device Driver code.
 *
 * @addtogroup I2C
 * @{
 */

#include "I2CDevice.h"


#include "ch.hpp"
#include "hal.h"
#include "chtypes.h"
#include "i2c.h"
#include "i2c_lld.h"


#include <stdint.h>
#include <stdbool.h>

I2CDevice::I2CDevice(I2CDriver *_i2cx, uint8_t address): _addr(address), _i2cx(_i2cx), m_timeout(MS2ST(100)), m_timeout_ms(100)
{
}

void I2CDevice::setAddress(uint8_t address)
{
    _addr = address;
}

uint8_t I2CDevice::getAddress()
{
    return _addr;
}

msg_t I2CDevice::readByte(uint8_t memAddress, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;

    #ifdef I2C_AQUIRE_BUS
        i2cAcquireBus(_i2cx);
    #endif

    transaction_status = i2cMasterTransmitTimeout(_i2cx, _addr, &memAddress, (uint8_t)1, data, (uint8_t)1, timeout);

    #ifdef I2C_AQUIRE_BUS
        i2cReleaseBus(_i2cx);
    #endif

    return transaction_status;
}

msg_t I2CDevice::readBit(uint8_t memAddress, uint8_t bitNum, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;

    #ifdef I2C_AQUIRE_BUS
        i2cAcquireBus(_i2cx);
    #endif

    transaction_status = i2cMasterTransmitTimeout(_i2cx, _addr, &memAddress, (uint8_t)1, &byte, (uint8_t)1, timeout);

    #ifdef I2C_AQUIRE_BUS
        i2cReleaseBus(_i2cx);
    #endif
    *data = byte & (1 << bitNum);

    return transaction_status;
}

msg_t I2CDevice::readBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;

    transaction_status = readByte(memAddress, &byte, timeout);
    if (transaction_status == RDY_OK)
    {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        byte &= mask;
        byte >>= (bitStart - length + 1);
        *data = byte;
    }

    return transaction_status;
}

msg_t I2CDevice::readBytes(uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    transaction_status = RDY_TIMEOUT;
    //if (memAddress != I2CDEV_NO_MEM_ADDR)
    //{
        #ifdef I2C_AQUIRE_BUS
            i2cAcquireBus(_i2cx);
        #endif

        transaction_status = i2cMasterTransmitTimeout(_i2cx, _addr, &memAddress, (uint8_t)1, data, (uint16_t)len, timeout);

        #ifdef I2C_AQUIRE_BUS
            i2cReleaseBus(_i2cx);
        #endif
    //}

    return transaction_status;
}

msg_t I2CDevice::writeBytes(uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t *buffer;
    int i;

    //if (memAddress != I2CDEV_NO_MEM_ADDR)
    //{
        // Sorry ...
        //if (len > 16) len = 16;

        if(len == 0) return RDY_OK;
        buffer = (uint8_t*) chHeapAlloc(NULL, (len+1)*sizeof(uint8_t));
        if (buffer == NULL)
            return RDY_OK; //falls kein speicher bekommen, dann gehe aus funktion raus

        buffer[0] = memAddress;
        for(i = 0; i < len ; i++)
          buffer[i + 1] = data[i];

        #ifdef I2C_AQUIRE_BUS
            i2cAcquireBus(_i2cx);
        #endif

        transaction_status = i2cMasterTransmitTimeout(_i2cx, _addr, buffer, (uint8_t)(len+1), NULL, (uint16_t)0, timeout);

        #ifdef I2C_AQUIRE_BUS
            i2cReleaseBus(_i2cx);
        #endif
        chHeapFree(buffer);
        buffer = NULL;
    //}
    //else
    //{
    //    #ifdef I2C_AQUIRE_BUS
    //        i2cAcquireBus(_i2cx);
    //    #endif

    //    transaction_status = i2cMasterTransmitTimeout(_i2cx, _addr, data, (uint8_t)(len), NULL, (uint16_t)0, timeout);

    //    #ifdef I2C_AQUIRE_BUS
    //        i2cReleaseBus(_i2cx);
    //    #endif
    //}

    return transaction_status;
}

msg_t I2CDevice::writeByte(uint8_t memAddress, uint8_t data, systime_t timeout)
{
    return writeBytes(memAddress, 1, &data, timeout);
}

msg_t I2CDevice::writeBit(uint8_t memAddress, uint8_t bitNum, uint8_t data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;
    transaction_status = readByte(memAddress, &byte, timeout);
    if (transaction_status != RDY_OK)
        return transaction_status;

    byte = (data != 0) ? (byte | (1 << bitNum)) : (byte & ~(1 << bitNum));
    return writeByte(memAddress, byte, timeout);
}

msg_t I2CDevice::writeBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;

    transaction_status = readByte(memAddress, &byte, timeout);
    if (transaction_status == RDY_OK)
    {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        data <<= (bitStart - length + 1); // shift data into correct position
        data &= mask; // zero all non-important bits in data
        byte &= ~(mask); // zero all important bits in existing byte
        byte |= data; // combine data with existing byte
        transaction_status = writeByte(memAddress, byte, timeout);
    }

    return transaction_status;
}

msg_t I2CDevice::writeWord(uint8_t memAddress, uint16_t data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t data8[2];

    data8[0] = (uint8_t)((data >> 8) & 0b11111111);
    data8[1] = (uint8_t)(data & 0b11111111);

    transaction_status = writeBytes(memAddress, 2, data8, timeout);

    return transaction_status;
}

msg_t I2CDevice::readByte(uint8_t devAddress, uint8_t memAddress, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;

    #ifdef I2C_AQUIRE_BUS
        i2cAcquireBus(_i2cx);
    #endif

    transaction_status = i2cMasterTransmitTimeout(_i2cx, devAddress, &memAddress, (uint8_t)1, data, (uint8_t)1, timeout);

    #ifdef I2C_AQUIRE_BUS
        i2cReleaseBus(_i2cx);
    #endif

    return transaction_status;
}

msg_t I2CDevice::readBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;

    #ifdef I2C_AQUIRE_BUS
        i2cAcquireBus(_i2cx);
    #endif

    transaction_status = i2cMasterTransmitTimeout(_i2cx, devAddress, &memAddress, (uint8_t)1, &byte, (uint8_t)1, timeout);

    #ifdef I2C_AQUIRE_BUS
        i2cReleaseBus(_i2cx);
    #endif
    *data = byte & (1 << bitNum);

    return transaction_status;
}

msg_t I2CDevice::readBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;

    transaction_status = readByte(devAddress, memAddress, &byte, timeout);
    if (transaction_status == RDY_OK)
    {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        byte &= mask;
        byte >>= (bitStart - length + 1);
        *data = byte;
    }

    return transaction_status;
}

msg_t I2CDevice::readBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    transaction_status = RDY_TIMEOUT;
    //if (memAddress != I2CDEV_NO_MEM_ADDR)
    //{
        #ifdef I2C_AQUIRE_BUS
            i2cAcquireBus(_i2cx);
        #endif

        transaction_status = i2cMasterTransmitTimeout(_i2cx, devAddress, &memAddress, (uint8_t)1, data, (uint16_t)len, timeout);

        #ifdef I2C_AQUIRE_BUS
            i2cReleaseBus(_i2cx);
        #endif
    //}

    return transaction_status;
}

msg_t I2CDevice::writeBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t *buffer;
    int i;

    //if (memAddress != I2CDEV_NO_MEM_ADDR)
    //{
        // Sorry ...
        //if (len > 16) len = 16;

        if(len == 0) return RDY_OK;
        buffer = (uint8_t*) chHeapAlloc(NULL, (len+1)*sizeof(uint8_t));
        if (buffer == NULL)
            return RDY_OK; //falls kein speicher bekommen, dann gehe aus funktion raus

        buffer[0] = memAddress;
        for(i = 0; i < len ; i++)
          buffer[i + 1] = data[i];

        #ifdef I2C_AQUIRE_BUS
            i2cAcquireBus(_i2cx);
        #endif

        transaction_status = i2cMasterTransmitTimeout(_i2cx, devAddress, buffer, (uint8_t)(len+1), NULL, (uint16_t)0, timeout);

        #ifdef I2C_AQUIRE_BUS
            i2cReleaseBus(_i2cx);
        #endif
        chHeapFree(buffer);
        buffer = NULL;
    //}
    //else
    //{
    //    #ifdef I2C_AQUIRE_BUS
    //        i2cAcquireBus(_i2cx);
    //    #endif

    //    transaction_status = i2cMasterTransmitTimeout(_i2cx, devAddress, data, (uint8_t)(len), NULL, (uint16_t)0, timeout);

    //    #ifdef I2C_AQUIRE_BUS
    //        i2cReleaseBus(_i2cx);
    //    #endif
    //}

    return transaction_status;
}

msg_t I2CDevice::writeByte(uint8_t devAddress, uint8_t memAddress, uint8_t data, systime_t timeout)
{
    return writeBytes(devAddress, memAddress, 1, &data, timeout);
}

msg_t I2CDevice::writeBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;
    transaction_status = readByte(devAddress, memAddress, &byte, timeout);
    if (transaction_status != RDY_OK)
        return transaction_status;

    byte = (data != 0) ? (byte | (1 << bitNum)) : (byte & ~(1 << bitNum));
    return writeByte(devAddress, memAddress, byte, timeout);
}

msg_t I2CDevice::writeBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t byte;

    transaction_status = readByte(devAddress, memAddress, &byte, timeout);
    if (transaction_status == RDY_OK)
    {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        data <<= (bitStart - length + 1); // shift data into correct position
        data &= mask; // zero all non-important bits in data
        byte &= ~(mask); // zero all important bits in existing byte
        byte |= data; // combine data with existing byte
        transaction_status = writeByte(devAddress, memAddress, byte, timeout);
    }

    return transaction_status;
}

msg_t I2CDevice::writeWord(uint8_t devAddress, uint8_t memAddress, uint16_t data, systime_t timeout)
{
    msg_t transaction_status;
    uint8_t data8[2];

    data8[0] = (uint8_t)((data >> 8) & 0b11111111);
    data8[1] = (uint8_t)(data & 0b11111111);

    transaction_status = writeBytes(devAddress, memAddress, 2, data8, timeout);


    return transaction_status;
}

void I2CDevice::timeout(uint32_t ms)
{
  m_timeout = MS2ST(ms);
  m_timeout_ms = ms;
}

uint32_t I2CDevice::timeout()
{
  return m_timeout_ms;
}

msg_t I2CDevice::readByte(uint8_t memAddress, uint8_t *data)
{
    return I2CDevice::readByte(memAddress, data, m_timeout);
}

msg_t I2CDevice::readBit(uint8_t memAddress, uint8_t bitNum, uint8_t *data)
{
    return I2CDevice::readBit(memAddress, bitNum, data, m_timeout);
}

msg_t I2CDevice::readBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data)
{
    return I2CDevice::readBits(memAddress, bitStart, length, data, m_timeout);
}

msg_t I2CDevice::readBytes(uint8_t memAddress, uint16_t len, uint8_t *data)
{
    return I2CDevice::readBytes(memAddress, len, data, m_timeout);
}

msg_t I2CDevice::writeBytes(uint8_t memAddress, uint16_t len, uint8_t *data)
{
    return I2CDevice::writeBytes(memAddress, len, data, m_timeout);
}

msg_t I2CDevice::writeByte(uint8_t memAddress, uint8_t data)
{
    return I2CDevice::writeByte(memAddress, data, m_timeout);
}

msg_t I2CDevice::writeBit(uint8_t memAddress, uint8_t bitNum, uint8_t data)
{
    return I2CDevice::writeBit(memAddress, bitNum, data, m_timeout);
}

msg_t I2CDevice::writeBits(uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data)
{
    return I2CDevice::writeBits(memAddress, bitStart, length, data, m_timeout);
}

msg_t I2CDevice::writeWord(uint8_t memAddress, uint16_t data)
{
    return I2CDevice::writeWord(memAddress, data, m_timeout);
}

msg_t I2CDevice::readByte(uint8_t devAddress, uint8_t memAddress, uint8_t *data)
{
    return I2CDevice::readByte(devAddress, memAddress, data, m_timeout);
}

msg_t I2CDevice::readBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t *data)
{
    return I2CDevice::readBit(devAddress, memAddress, bitNum, data, m_timeout);
}

msg_t I2CDevice::readBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t *data)
{
    return I2CDevice::readBits(devAddress, memAddress, bitStart, length, data, m_timeout);
}

msg_t I2CDevice::readBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data)
{
    return I2CDevice::readBytes(devAddress, memAddress, len, data, m_timeout);
}

msg_t I2CDevice::writeBytes(uint8_t devAddress, uint8_t memAddress, uint16_t len, uint8_t *data)
{
    return I2CDevice::writeBytes(devAddress, memAddress, len, data, m_timeout);
}

msg_t I2CDevice::writeByte(uint8_t devAddress, uint8_t memAddress, uint8_t data)
{
    return I2CDevice::writeByte(devAddress, memAddress, data, m_timeout);
}

msg_t I2CDevice::writeBit(uint8_t devAddress, uint8_t memAddress, uint8_t bitNum, uint8_t data)
{
    return I2CDevice::writeBit(devAddress, memAddress, bitNum, data, m_timeout);
}

msg_t I2CDevice::writeBits(uint8_t devAddress, uint8_t memAddress, uint8_t bitStart, uint8_t length, uint8_t data)
{
    return I2CDevice::writeBits(devAddress, memAddress, bitStart, length, data, m_timeout);
}

msg_t I2CDevice::writeWord(uint8_t devAddress, uint8_t memAddress, uint16_t data)
{
    return I2CDevice::writeWord(devAddress, memAddress, data, m_timeout);
}
