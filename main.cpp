/*
    Main - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Revisions:
 * 	Date		Version			Author			Description
 *  24.04.2014	0.1				Rothfelder		First version. ChibiOS blinky Demo.
 */
#include "ch.hpp"
#include "hal.h"
#include "chconf.h"
#include <pal.h>
#include "stm32f4xx.h"
#include "init_hw.h"
#include "chprintf.h"
#include "ahrs.hpp"
#include "quaternion.hpp"
#include "threads.hpp"

extern "C"
{
#include "myUSB.h"
#include "shell.h"
}
#include "shell_commands.hpp"
#include "sensors.hpp"
#include "nrf24l01.hpp"
#include "at25eeprom.hpp"

using namespace chibios_rt;

/* Static threads instances.*/
static threads::StabilizeThread stabilize;
/* Static threads instances.*/
static threads::BlinkerThread blinker;
static threads::MotorsThread motorthread;
static threads::BatteryThread battery;
static threads::IdleThread idle;

static void mpuInterrupt(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  /* Wakes up the thread.*/
  chibios_rt::System::lockFromIsr();
  stabilize.signalEventsI((eventmask_t)1);
  chibios_rt::System::unlockFromIsr();

}

static void buttonPressed(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  /* Wakes up the thread.*/
  chibios_rt::System::lockFromIsr();
  threads::calc_offset = true;
  chibios_rt::System::unlockFromIsr();

}

static void nrf24l01Interrupt(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;

  /* Wakes up the thread.*/
  chibios_rt::System::lockFromIsr();
  chibios_rt::System::unlockFromIsr();

}

static const EXTConfig extConfig = {{
{ EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOA , buttonPressed } ,
{ EXT_CH_MODE_FALLING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB , nrf24l01Interrupt } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_RISING_EDGE | EXT_CH_MODE_AUTOSTART | EXT_MODE_GPIOB , mpuInterrupt } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
{ EXT_CH_MODE_DISABLED , NULL } ,
}};


int main(void) {
  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  System::init();
  shellInit();

  INIT_HW::GPIO();
  INIT_HW::I2C();
  INIT_HW::SPI();
  INIT_HW::UART();
  INIT_HW::USB();
  INIT_HW::ADConverter();
  idle.start(NORMALPRIO);
  blinker.start(NORMALPRIO);
  battery.start(NORMALPRIO);

  stabilize.start(HIGHPRIO);
  extStart( &EXTD1 , &extConfig );
  extChannelEnable(&EXTD1, 0);
  extChannelEnable(&EXTD1, 1);
  extChannelEnable(&EXTD1, 7);

  modules::sensors::SENSORS();

  motorthread.start(NORMALPRIO);


  SPIConfig spiCfgNrf24l01pSpionfig {
    NULL,
    GPIOB,
    2,
    0
  };
  SPIConfig spiCfgEeprom {
    NULL,
    GPIOA,
    15,
    SPI_CR1_BR_0
  };
  driver::At25eeprom eeprom(&SPID2, spiCfgEeprom);
  /*
  INIT_HW::IoPin ce;
  ce.port = GPIOB;
  ce.pin  = GPIOB_PIN5;
  modules::Nrf24l01p nrf24l01p(&SPID2, spiCfgNrf24l01pSpionfig, ce);

  uint8_t status = nrf24l01p.Get_status();
  */
  uint16_t address = 10;
  uint8_t val = 5;
  eeprom.WriteEnabled(true);
  driver::At25eeprom::StatusReg reg = eeprom.StatusRegister();
  eeprom.Write(address,val);
  chibios_rt::BaseThread::sleep(MS2ST(15));
  uint8_t test = eeprom.Read(address);
  //inter.start(NORMALPRIO + 10);

    /*
    * Main loop, does nothing except spawn a shell when the old one was terminated
    */
      while (TRUE) {
        /*
        struct EulerAngles euler;
        Quaternion q = ahrs.Orientation();
        euler = q.euler();
        chprintf(reinterpret_cast<BaseSequentialStream *>(&SD3), "euler: roll:%05.1f pitch:%05.1f yaw:%05.1f \r\n", euler.roll*57.296, euler.pitch*57.296, euler.yaw*57.296);
        chprintf(reinterpret_cast<BaseSequentialStream *>(&SD3), "quaternion: %.3f %.3f %.3f %.3f\r\n", q.w, q.x, q.y, q.z);*/
        if (!shell::th_shell && isUsbActive())
          shell::th_shell = shellCreate(&shell::config, 4096, NORMALPRIO);
        else if (chThdTerminated(shell::th_shell)) {
          chThdRelease(shell::th_shell); /* Recovers memory of the previous shell. */
          shell::th_shell = NULL; /* Triggers spawning of a new shell. */
        }
        BaseThread::sleep(MS2ST(100));
      }
}
