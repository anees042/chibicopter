OpenQuadro
==========
License information:
All open source files in this repository are licensed under the GNU General Public Licence (either version 3 of the License, or [at your option] any later version) unless an alternative licence is provided in source.

OpenQuadro implements a flight controller based on a STM32F4 Discovery board for a self made quadrotor. 
