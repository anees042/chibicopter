/*
    PID Class - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Revisions:
 *  Date        Version         Author          Description
 *  25.04.2014  0.1             Rothfelder      First version
 */


/**
 * @file    PID.cpp
 * @brief   PID Class source file.
 *
 * @addtogroup PID
 * @{
 */
#include <pid.hpp>
#include "ch.hpp"
#include "chconf.h"
#include "hal.h"
#include "chtypes.h"
#include "math.h"

namespace modules {
  PID::PID(struct PIDtunings tunings, enum PIDdirection direction, float sampleFreq) : m_integratedWeightedError(0), m_lastInput(0), m_semaphore(true)
  {
    PIDlimits limits;
    limits.high = 0;
    limits.low = 0;

    this->Reset();
    this->Setpoint(0.0f);
    this->OutputLimits(limits);
    this->OutputLimitsActive(false);
    this->Direction(direction);
    this->SampleFreq(sampleFreq);
    this->Tunings(tunings);
    this->WindUpGuard(0.0f);
    this->WindUpGuardActive(false);
    this->Unlock();
  }

  PID::PID(struct PIDtunings tunings, enum PIDdirection direction, float sampleFreq, float setpoint) : m_integratedWeightedError(0), m_lastInput(0), m_semaphore(true)
  {
    PIDlimits limits;
    limits.high = 0;
    limits.low = 0;

    this->Reset();
    this->Setpoint(setpoint);
    this->OutputLimits(limits);
    this->OutputLimitsActive(false);
    this->Direction(direction);
    this->SampleFreq(sampleFreq);
    this->Tunings(tunings);
    this->WindUpGuard(0.0f);
    this->WindUpGuardActive(false);
    this->Unlock();
  }

  PID::PID(struct PIDtunings tunings, enum PIDdirection direction, float sampleFreq, struct PIDlimits limits) : m_integratedWeightedError(0), m_lastInput(0), m_semaphore(true)
  {
    this->Reset();
    this->Setpoint(0.0f);
    this->OutputLimits(limits);
    this->Direction(direction);
    this->SampleFreq(sampleFreq);
    this->Tunings(tunings);
    this->WindUpGuard(0.0f);
    this->WindUpGuardActive(false);
    this->Unlock();
  }

  PID::PID(struct PIDtunings tunings, enum PIDdirection direction, float sampleFreq, float setpoint, struct PIDlimits limits) : m_integratedWeightedError(0), m_lastInput(0), m_semaphore(true)
  {
    this->Reset();
    this->Setpoint(setpoint);
    this->OutputLimits(limits);
    this->Direction(direction);
    this->SampleFreq(sampleFreq);
    this->Tunings(tunings);
    this->WindUpGuard(0.0f);
    this->WindUpGuardActive(false);
    this->Unlock();
  }

  float PID::Compute(float input)
  {
    float error = this->m_setpoint - input;
    float dTerm;

    if (this->m_wasReset)
    {
      this->m_lastInput = this->m_setpoint; //oder vllt setpoint
      this->m_wasReset = false;
    }

    this->m_integratedWeightedError += error * this->m_tuningsWeighted.ki;   //store integral

    if (this->m_windupguardactive)
    {
      if (fabs(this->m_integratedWeightedError) > this->m_windupguard)
      {
        if (this->m_integratedWeightedError > 0.0f)
          this->m_integratedWeightedError = this->m_windupguard;
        else
          this->m_integratedWeightedError = -1.0 * this->m_windupguard;
      }
    }

    dTerm = (input - this->m_lastInput)*this->m_tuningsWeighted.kd;
    this->m_lastInput = input;

    this->m_output = (error * this->m_tuningsWeighted.kp) + this->m_integratedWeightedError - dTerm;
    if (this->m_limitoutputs)
    {
      if (this->m_output > this->m_limits.high)
        this->m_output = this->m_limits.high;
      else if (this->m_output < this->m_limits.low)
        this->m_output = this->m_limits.low;
    }
    return this->m_output;
  }

  float PID::Compute(float input, float setpoint)
  {
    this->m_setpoint = setpoint;
    return this->Compute(input);
  }

  void PID::Tunings(struct PID::PIDtunings newTunings)
  {
    this->m_tunings = newTunings;
    this->m_tuningsWeighted.kp = newTunings.kp;
    this->m_tuningsWeighted.ki = newTunings.ki * this->m_sampleperiod;
    this->m_tuningsWeighted.kd = newTunings.kd / this->m_sampleperiod;

    if (this->Direction() == PIDdirection::REVERSE)
    {
      this->m_tuningsWeighted.kp = 0 - this->m_tuningsWeighted.kp;
      this->m_tuningsWeighted.ki = 0 - this->m_tuningsWeighted.ki;
      this->m_tuningsWeighted.kd = 0 - this->m_tuningsWeighted.kd;
    }
  }

  struct PID::PIDtunings PID::Tunings()
  {
    return this->m_tunings;
  }

  void PID::SamplePeriod(float ms)
  {
    this->m_sampleperiod = ms/1000;
    this->Tunings(this->m_tunings);
  }

  float PID::SamplePeriod()
  {
    return this->m_sampleperiod*1000;
  }

  void PID::SampleFreq(float hz)
  {
    this->m_sampleperiod = 1/hz;
    this->Tunings(this->m_tunings);
  }

  float PID::SampleFreq()
  {
    return 1/this->m_sampleperiod;
  }

  void PID::OutputLimits(struct PIDlimits newLimits)
  {
    this->m_limits = newLimits;
    this->OutputLimitsActive(true);
  }

  struct PID::PIDlimits PID::OutputLimits()
  {
    return this->m_limits;
  }

  void PID::OutputLimitsActive(bool activate)
  {
    this->m_limitoutputs = activate;
  }

  bool PID::OutputLimitsActive()
  {
    return this->m_limitoutputs;
  }

  void PID::Direction(enum PID::PIDdirection newDirection)
  {
    if (this->m_direction != newDirection)
    {
      this->m_direction = newDirection;
      this->Tunings(this->m_tunings);
    }
  }

  enum PID::PIDdirection PID::Direction()
  {
    return this->m_direction;
  }

  void PID::Kp(float kp)
  {
    this->m_tunings.kp = kp;
    this->Tunings(this->m_tunings);
  }

  float PID::Kp()
  {
    return this->m_tunings.kp;
  }

  void PID::Ki(float ki)
  {
    this->m_tunings.ki = ki;
    this->Tunings(this->m_tunings);
  }

  float PID::Ki()
  {
    return this->m_tunings.ki;
  }

  void PID::Kd(float kd)
  {
    this->m_tunings.kd = kd;
    this->Tunings(this->m_tunings);
  }

  float PID::Kd()
  {
    return this->m_tunings.kd;
  }

  void PID::Setpoint(float setpoint)
  {
    this->m_setpoint = setpoint;
  }

  float PID::Setpoint()
  {
    return this->m_setpoint;
  }

  float PID::Output()
  {
    return this->m_output;
  }

  void PID::Reset()
  {
    this->m_wasReset = true;
    this->m_lastInput = this->m_setpoint;
    this->m_integratedWeightedError = 0;
    this->m_output = 0;
  }

  void PID::WindUpGuard(float value)
  {
    if (value < 0)
      value *= -1.0f;
    this->m_windupguard = value;
    this->WindUpGuardActive(true);
  }

  float PID::WindUpGuard(void)
  {
    return this->m_windupguard;
  }

  void PID::WindUpGuardActive(bool enabled)
  {
    this->m_windupguardactive = enabled;
  }

  bool PID::WindUpGuardActive(void)
  {
    return this->m_windupguardactive;
  }

  void PID::Lock(void)
  {
    this->m_semaphore.wait();
  }

  void PID::Unlock(void)
  {
    this->m_semaphore.signal();
  }
}

/*! @} */
