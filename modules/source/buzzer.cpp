/*
 * buzzer.cpp
 *
 *  Created on: 13.06.2014
 *      Author: tobias
 */

#include "ch.hpp"
#include "chconf.h"
#include "hal.h"
#include "pal.h"
#include "chtypes.h"
#include "buzzer.hpp"

modules::Buzzer::Buzzer(void) : m_semaphore(true)
{
  this->Off();
  this->Unlock();
}

void modules::Buzzer::On(void)
{
  palSetPad(GPIOD, 1);
}

void modules::Buzzer::Off(void)
{
  palClearPad(GPIOD, 1);
}

void modules::Buzzer::Test(void)
{
  palSetPad(GPIOD, 1);
  chibios_rt::BaseThread::sleep(MS2ST(500));
  palClearPad(GPIOD, 1);
}

void modules::Buzzer::Lock(void)
{
  this->m_semaphore.wait();
}

void modules::Buzzer::Unlock(void)
{
  this->m_semaphore.signal();
}


