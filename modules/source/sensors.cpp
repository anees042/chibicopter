/*
    I2CDevice Class - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Revisions:
 * 	Date		Version			Author			Description
 *  24.04.2014	0.1				Rothfelder		Init file
 */

/*!
 * @file    init_hw.cpp
 * @brief   Source file for initialization functions.
 *
 * @addtogroup Init
 * @{
 */

#include "sensors.hpp"

#include "ch.hpp"
#include "chconf.h"
#include "hal.h"
#include "chtypes.h"


#include "MPU6050.h"
#include "HMC5883L.hpp"
#include "BMP085.hpp"


namespace modules {

  namespace sensors {

    MPU6050 mpu(&I2CD1, true);
    HMC5883L magnetometer(&I2CD1);
    BMP085 pressure(&I2CD1);

    void SENSORS()
    {
        //uint16_t tmp;
        //mpu.setSleepEnabled(true);
        mpu.reset();
        chibios_rt::BaseThread::sleep(MS2ST(100));
        mpu.setSleepEnabled(false);
        mpu.setClockSource(MPU6050_CLOCK_PLL_XGYRO);
        //mpu.setClockSource(MPU6050_CLOCK_INTERNAL);
        //mpu.reset();
        //mpu.setClockSource(MPU6050_CLOCK_INTERNAL);
        mpu.setFullScaleGyroRange(MPU6050_GYRO_FS_2000);
        mpu.setFullScaleAccelRange(MPU6050_ACCEL_FS_16);


        mpu.setDLPFMode(MPU6050_DLPF_BW_188);
        mpu.setRate(1);

        /*
        //setup magnetometer
        mpu.setI2CMasterModeEnabled(false);
        mpu.setI2CBypassEnabled(true);
        //mpu.writeMagnetometerByte(0x0Au, 0x08u); //selftest modus
        mpu.writeMagnetometerByte(0x0Au, 0x01u); //single messurement modus
        chibios_rt::BaseThread::sleep(MS2ST(100));
        mpu.setI2CBypassEnabled(false);

        //set magnetometer to slv0
        mpu.setSlaveAddress(0, 0x8Cu);
        mpu.setSlaveRegister(0, 0x03u);
        mpu.setSlaveDataLength(0, 0x06u);
        mpu.setMasterClockSpeed(static_cast<uint8_t>(13u));
        mpu.setI2CMasterModeEnabled(true);
        mpu.setWaitForExternalSensorEnabled(true);
        */

        mpu.setInterruptLatchClear(false);
        mpu.setTempSensorEnabled(true);
        mpu.setAccelFIFOEnabled(true);
        mpu.setTempFIFOEnabled(true);
        mpu.setXGyroFIFOEnabled(true);
        mpu.setYGyroFIFOEnabled(true);
        mpu.setZGyroFIFOEnabled(true);
        //mpu.setSlave0FIFOEnabled(true);



        mpu.setInterruptMode(false);
        //mpu.setIntFIFOBufferOverflowEnabled(true);
        //mpu.setIntDataReadyEnabled(true);
        uint8_t interrupts = (1<<4) | (1<<0);
        mpu.setFIFOEnabled(true);
        mpu.resetFIFO();
        //tmp = mpu.getFIFOCount();
        //while (tmp-- > 0)
        //  mpu.getFIFOByte();
        //chibios_rt::BaseThread::sleep(MS2ST(50));
        mpu.setFIFOEnabled(true);
        mpu.setIntEnabled(interrupts);


        //magnetometer.initialize();
    }
  }
}
