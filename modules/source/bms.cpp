/*
 * bms.cpp
 *
 *  Created on: 13.06.2014
 *      Author: tobias
 */

#include "bms.hpp"
#include "ch.hpp"
#include "hal.h"
#include "chprintf.h"
#include "init_hw.h"
#include "adc.h"


modules::BMS::BMS(const ADCConversionGroup* group, ADCDriver* adcd) :  m_adcd(adcd), m_cell1Threshold(3.2f), m_cell2Threshold(3.2f), m_cell3Threshold(3.2f), m_group(group), m_semaphore(false)
{
}

modules::BMS::BMS(const ADCConversionGroup* group, ADCDriver* adcd, float cell1Threshold, float cell2Threshold, float cell3Threshold) : m_adcd(adcd), m_cell1Threshold(cell1Threshold), m_cell2Threshold(cell2Threshold), m_cell3Threshold(cell3Threshold), m_group(group), m_semaphore(false)
{
}

void modules::BMS::Update(void)
{
  adcsample_t samples[3 * 10];
  float cell1 = 0.0,cell2 = 0.0,cell3 = 0.0;
  adcConvert(this->m_adcd, this->m_group, samples, 10);

  for (unsigned int i = 0; i < 10; i++)
  {
    cell1 += static_cast<float>(samples[i*3  ]) / (10 * 4095.0) * 4.5 * 1.0;
    cell2 += static_cast<float>(samples[i*3+1]) / (10 * 4095.0) * 4.5 * 2.0;
    cell3 += static_cast<float>(samples[i*3+2]) / (10 * 4095.0) * 4.5 * 3.0;
  }

  this->m_cell3 = cell3 - cell2;
  this->m_cell2 = cell2 - cell1;
  this->m_cell1 = cell1;

  this->m_stateOk = (this->m_cell1 > this->m_cell1Threshold) and (this->m_cell2 > this->m_cell2Threshold) and (this->m_cell3 > this->m_cell3Threshold);
}

float modules::BMS::Cell1(void)
{
  return this->m_cell1;
}

float modules::BMS::Cell2(void)
{
  return this->m_cell2;
}

float modules::BMS::Cell3(void)
{
  return this->m_cell3;
}

float modules::BMS::Cell1Threshold(void)
{
  return this->m_cell1Threshold;
}

void modules::BMS::Cell1Threshold(float newThreshold)
{
  if (newThreshold < 3.0f)
    newThreshold = 3.0f;
  this->m_cell1Threshold = newThreshold;
}

float modules::BMS::Cell2Threshold(void)
{
  return this->m_cell2Threshold;
}

void modules::BMS::Cell2Threshold(float newThreshold)
{
  if (newThreshold < 3.0f)
    newThreshold = 3.0f;
  this->m_cell2Threshold = newThreshold;
}

float modules::BMS::Cell3Threshold(void)
{
  return this->m_cell3Threshold;
}

void modules::BMS::Cell3Threshold(float newThreshold)
{
  if (newThreshold < 3.0f)
    newThreshold = 3.0f;
  this->m_cell3Threshold = newThreshold;
}

bool modules::BMS::StateOk(void)
{
  return this->m_stateOk;
}

void modules::BMS::Lock(void)
{
  this->m_semaphore.wait();
}

void modules::BMS::Unlock(void)
{
  this->m_semaphore.signal();
}
