/*
 * shell_commands.cpp
 *
 *  Created on: 08.06.2014
 *      Author: tobias
 */

#include "shell_commands.hpp"
#include "threads.hpp"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ch.hpp"
#include "hal.h"
#include "chprintf.h"
#include "init_hw.h"
extern "C" {
  #include "shell.h"
  #include "myUSB.h"
#include "adc.h"
}

Thread* shell::th_shell = NULL;


void shell::cmd_calibrate(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  if (argc > 0)
  {
    chprintf(chp, "Usage: calibrate\r\n");
    return;
  }
  threads::calc_offset = true;
  chprintf(chp, "Don't move me. I am calibration now.\r\n");
}

void shell::cmd_arm(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  if (argc != 1)
  {
    chprintf(chp, "Usage: arm true/false\r\n");
    return;
  }
  if (strcmp(argv[0], "true") == 0)
  {
    threads::arm = true;
    chprintf(chp, "Caution! Motors turned on now!\r\n");
    return;
  }
  if (strcmp(argv[0], "false") == 0)
  {
    threads::arm = false;
    chprintf(chp, "Motors turned off.\r\n");
    return;
  }
  chprintf(chp, "Usage: arm true/false\r\n");
  return;
}

void shell::cmd_gas(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  if (argc == 0)
  {
    chprintf(chp, "Gas: %d.\r\n", threads::gas);
    return;
  }
  if (argc != 1)
  {
    chprintf(chp, "Usage: gas value.\r\n");
    return;
  }

  if ( strcmp(argv[0], "test") == 0)
  {
    threads::motors.Lock();
    threads::motors.Test();
    threads::motors.Unlock();
    return;
  }

  int val = atoi(argv[0]);
  threads::motors.Lock();
  if ( (val < threads::motors.min()) || (val > static_cast<uint8_t>(threads::motors.max()/1.3f)) )
  {
    chprintf(chp, "Usage: Value must be >=%d and <=%d.\r\n",threads::motors.min(), static_cast<uint8_t>(threads::motors.max()/1.3f));
    threads::motors.Unlock();
    return;
  }
  threads::motors.Unlock();
  threads::gas = static_cast<uint8_t>(val);
  chprintf(chp, "Set motor gas to %d.\r\n", threads::gas);
  return;
}

void shell::cmd_stabilize(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  if (argc != 1)
  {
    chprintf(chp, "Usage: stabilize angle\r\n");
    return;
  }
  if (strcmp(argv[0], "roll") == 0)
  {
    threads::stabilizeAngle = threads::StabilizeAngle::roll;
    chprintf(chp, "Turned stabilization to roll angle.\r\n");
    return;
  }
  if (strcmp(argv[0], "pitch") == 0)
  {
    threads::stabilizeAngle = threads::StabilizeAngle::pitch;
    chprintf(chp, "Turned stabilization to pitch angle.\r\n");
    return;
  }
  if (strcmp(argv[0], "yaw") == 0)
  {
    threads::stabilizeAngle = threads::StabilizeAngle::yaw;
    chprintf(chp, "Turned stabilization to yaw.\r\n");
    return;
  }
  if (strcmp(argv[0], "both") == 0)
  {
    threads::stabilizeAngle = threads::StabilizeAngle::both;
    chprintf(chp, "Turned stabilization to both, roll and pitch angle.\r\n");
    return;
  }
  if (strcmp(argv[0], "all") == 0)
  {
    threads::stabilizeAngle = threads::StabilizeAngle::all;
    chprintf(chp, "Turned stabilization to all angles.\r\n");
    return;
  }
  chprintf(chp, "Usage: stabilize roll/pitch/both\r\n");
  return;
}

void shell::cmd_print(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  if (argc == 0)
  {
    chprintf(chp, "Usage: print what\r\n");
    return;
  }
  if (strcmp(argv[0], "orientation") == 0)
  {
    threads::print_orientation = not threads::print_orientation;
    struct EulerAngles euler;
    threads::ahrs.Lock();
    Quaternion q = threads::ahrs.Orientation();
    threads::ahrs.Unlock();

    euler = q.euler();
    chprintf(chp, "euler: roll:%05.1f pitch:%05.1f yaw:%05.1f \r\n", euler.roll*57.296, euler.pitch*57.296, euler.yaw*57.296);
    chprintf(chp, "quaternion: %.3f %.3f %.3f %.3f\r\n", q.w, q.x, q.y, q.z);
    if (threads::print_orientation)
      chprintf(chp, "Printing of orientation quaternion to debug UART turned on.\r\n");
    else
      chprintf(chp, "Printing of orientation quaternion to debug UART turned off.\r\n");
    return;
  }
  chprintf(chp, "Usage: print what\r\n");
  return;
}

void shell::cmd_pid(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  if ( (argc != 1) and (argc != 4) )
  {
    chprintf(chp, "Usage: pid angle [kp ki kd]\r\n");
    return;
  }

  modules::PID::PIDtunings tunings;
  if (argc == 4)
  {
    tunings.kp = atof(argv[1]);
    tunings.ki = atof(argv[2]);
    tunings.kd = atof(argv[3]);
  }

  if (strcmp(argv[0], "roll") == 0 )
  {
    if (argc == 4)
    {
      threads::pidRoll.Lock();
      threads::pidRoll.Tunings(tunings);
      threads::pidRoll.Unlock();
      chprintf(chp, "Set roll pid values to: kp=%f ki=%f kd=%f\r\n",tunings.kp, tunings.ki, tunings.kd);
      return;
    } else
    {
      threads::pidRoll.Lock();
      tunings = threads::pidRoll.Tunings();
      threads::pidRoll.Unlock();
      chprintf(chp, "Roll pid values are: kp=%f ki=%f kd=%f\r\n",tunings.kp, tunings.ki, tunings.kd);
      return;
    }
    return;
  }

  if (strcmp(argv[0], "pitch") == 0 )
  {
    if (argc == 4)
    {
      threads::pidPitch.Lock();
      threads::pidPitch.Tunings(tunings);
      threads::pidPitch.Unlock();
      chprintf(chp, "Set pitch pid values to: kp=%f ki=%f kd=%f\r\n",tunings.kp, tunings.ki, tunings.kd);
      return;
    } else {
      threads::pidPitch.Lock();
      tunings = threads::pidPitch.Tunings();
      threads::pidPitch.Unlock();
      chprintf(chp, "Pitch pid values are: kp=%f ki=%f kd=%f\r\n",tunings.kp, tunings.ki, tunings.kd);
      return;
    }
    return;
  }

  if (strcmp(argv[0], "yaw") == 0 )
  {
    if (argc == 4)
    {
      threads::pidYaw.Lock();
      threads::pidYaw.Tunings(tunings);
      threads::pidYaw.Unlock();
      chprintf(chp, "Set yaw pid values to: kp=%f ki=%f kd=%f\r\n",tunings.kp, tunings.ki, tunings.kd);
      return;
    } else {
      threads::pidYaw.Lock();
      tunings = threads::pidYaw.Tunings();
      threads::pidYaw.Unlock();
      chprintf(chp, "Yaw pid values are: kp=%f ki=%f kd=%f\r\n",tunings.kp, tunings.ki, tunings.kd);
    return;
    }
    return;
  }

  chprintf(chp, "Usage: pid angle [kp ki kd]\r\n");
}

void shell::cmd_akku(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  (void)argc;
  threads::bms.Lock();
  chprintf(chp, "Battery Status: %s\r\n", threads::bms.StateOk() ? "Good" : "Bad");
  chprintf(chp, "Cell 1: %f\r\n", threads::bms.Cell1());
  chprintf(chp, "Cell 2: %f\r\n", threads::bms.Cell2());
  chprintf(chp, "Cell 3: %f\r\n", threads::bms.Cell3());
  threads::bms.Unlock();
  return;
}


void shell::cmd_buzzer(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argv;
  if ( (argc == 1) && (strcmp("test", argv[0])==0) )
  {
    threads::buzzer.Lock();
    threads::buzzer.Test();
    threads::buzzer.Unlock();
    return;
  }
  chprintf(chp, "Usage: buzzer [test]\r\n");
}

void shell::cmd_angle(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argc;
  (void)argv;

  if (argc == 0)
  {
    chprintf(chp, "Desired Angles: \r\n");
    chprintf(chp, "Roll: %f\r\n",threads::desiredAngles.roll);
    chprintf(chp, "Pitch: %f\r\n",threads::desiredAngles.pitch);
    chprintf(chp, "Yaw: %f\r\n",threads::desiredAngles.yaw);
    return;
  }

  if (argc == 2)
  {
    float val = atoi(argv[1]);
    if ( (strcmp("roll", argv[0]) == 0) )
    {
      threads::desiredAngles.roll = val;
      chprintf(chp, "Set roll angle to %f.\r\n", threads::desiredAngles.roll);
      return;
    }

    if ( (strcmp("pitch", argv[0]) == 0) )
    {
      threads::desiredAngles.pitch = val;
      chprintf(chp, "Set pitch angle to %f.\r\n", threads::desiredAngles.pitch);
      return;
    }

    if ( (strcmp("yaw", argv[0]) == 0) )
    {
      threads::desiredAngles.yaw = val;
      chprintf(chp, "Set yaw angle to %f.\r\n", threads::desiredAngles.yaw);
      return;
    }
    return;
  }
  chprintf(chp, "Usage: angle {roll/pitch/yaw} {wert}\r\n");
}
