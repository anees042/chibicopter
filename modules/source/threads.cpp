/*
 * threads.cpp
 *
 *  Created on: 08.06.2014
 *      Author: tobias
 */
#include "ch.hpp"
#include "hal.h"
#include "chconf.h"
#include <pal.h>
#include "stm32f4xx.h"
#include "init_hw.h"
#include "chprintf.h"
#include "ahrs.hpp"
#include "quaternion.hpp"
#include "sensors.hpp"
#include "threads.hpp"
#include "pid.hpp"
#include "motorcontroller.hpp"
#include "bms.hpp"
#include "buzzer.hpp"
#include "shell_commands.hpp"
extern "C"
{
  #include "myUSB.h"
  #include "shell.h"
}

#define AVERAGE 1000u
#define MPU_FIFO_BYTES 14u

namespace threads {
  StabilizeAngle stabilizeAngle = threads::StabilizeAngle::roll;
  modules::MadgwickAHRS ahrs(500.0f, 0.05);
  bool arm = false;
  bool calc_offset = true;
  uint8_t gas = 0;
  modules::PID pidPitch(modules::PID::PIDtunings(0.6f, 0.2f, 0.2f), modules::PID::PIDdirection::NORMAL, 200.0f);
  modules::PID pidRoll(modules::PID::PIDtunings(0.6f, 0.2f, 0.2f), modules::PID::PIDdirection::NORMAL, 200.0f);
  modules::PID pidYaw(modules::PID::PIDtunings(3.0f, 0.0f, 0.0f), modules::PID::PIDdirection::NORMAL, 200.0f);
  driver::MotorController motors(&SD1);
  bool print_orientation = false;
  modules::BMS bms(&INIT_HW::adcgrpcfg, &ADCD1, 3.1f, 3.1f, 3.1f);
  modules::Buzzer buzzer;
  EulerAngles desiredAngles = { 0.0f, 0.0f, 0.0f};

  /*
  * Tester thread class. This thread executes the test suite.
  */
  msg_t StabilizeThread::main(void) {
    uint8_t status;
    uint8_t buffer[1024];
    float gyr_x_avr = 0, gyr_y_avr = 0, gyr_z_avr = 0, temp_avr = 0, acc_x_avr = 0, acc_y_avr = 0, acc_z_avr = 0;
    float gyr_x_offset = 0, gyr_y_offset = 0, gyr_z_offset = 0, acc_x_offset = 0, acc_y_offset = 0, acc_z_offset = 0;
    uint32_t zaehler = 0;
    this->setName("Stabilize");

    while (TRUE) {
      /* Checks if an IRQ happened else wait.*/
      this->waitAnyEvent((eventmask_t)1);
      /* Perform processing here.*/
      status = modules::sensors::mpu.getIntStatus();
      if (status&(1<<4)) //overflow
      {
        modules::sensors::mpu.resetFIFO();
        //BaseThread::sleep(MS2ST(10));
        modules::sensors::mpu.setFIFOEnabled(true);
        palSetPad(GPIOD, 13);//ORANGE
        //chprintf(reinterpret_cast<BaseSequentialStream *>(&SD3), "Fifo overflow!!!\n\r");
      }
      else
      {
        uint16_t count = 0;
        //char strBuff[20];
        float acc_x, acc_y, acc_z, temp, gyr_x, gyr_y, gyr_z, mag_x, mag_y, mag_z;
        int16_t acc_x_int, acc_y_int, acc_z_int, temp_int, gyr_x_int, gyr_y_int, gyr_z_int, mag_x_int, mag_y_int, mag_z_int;
        count = modules::sensors::mpu.getFIFOCount();
        modules::sensors::mpu.getFIFOBytes(buffer, count);

        if (count%MPU_FIFO_BYTES==0) //(count%20 == 0)
        {
          for (uint32_t i=0; i<count/MPU_FIFO_BYTES;i++)
          {
            uint32_t offset = i*MPU_FIFO_BYTES;
            acc_x_int    = (static_cast<int16_t>(buffer[ 0 + offset]) << 8) | buffer[ 1 + offset];
            acc_y_int    = (static_cast<int16_t>(buffer[ 2 + offset]) << 8) | buffer[ 3 + offset];
            acc_z_int    = (static_cast<int16_t>(buffer[ 4 + offset]) << 8) | buffer[ 5 + offset];
            temp_int     = (static_cast<int16_t>(buffer[ 6 + offset]) << 8) | buffer[ 7 + offset];
            gyr_x_int    = (static_cast<int16_t>(buffer[ 8 + offset]) << 8) | buffer[ 9 + offset];
            gyr_y_int    = (static_cast<int16_t>(buffer[10 + offset]) << 8) | buffer[11 + offset];
            gyr_z_int    = (static_cast<int16_t>(buffer[12 + offset]) << 8) | buffer[13 + offset];
            //mag_x_int    = (static_cast<int16_t>(buffer[14 + offset]) << 8) )       | buffer[15 + offset]; //liefert erst x msb dann lsb
            //mag_z_int    = (static_cast<int16_t>(buffer[16 + offset]) << 8) )       | buffer[17 + offset]; //dann z msb dann lsb
            //mag_y_int    = (static_cast<int16_t>(buffer[18 + offset]) << 8) )       | buffer[19 + offset]; //dann y msb dann lsb


            acc_y = static_cast<float>( acc_x_int - acc_x_offset)  / -1;//-16384.0;
            acc_x = static_cast<float>( acc_y_int - acc_y_offset)  / -1;//16384.0;
            acc_z = static_cast<float>( acc_z_int - acc_z_offset)  /  1;//16384.0;
            temp =  static_cast<float>( temp_int )  / 340.0 + 36.53;
            gyr_y = static_cast<float>( gyr_x_int - gyr_x_offset)  /  16.4 / 57.2957;// / 7505.75);   //adding or substrating offsets
            gyr_x = static_cast<float>( gyr_y_int - gyr_y_offset)  /  16.4 / 57.2957;// / 7505.75);
            gyr_z = static_cast<float>( gyr_z_int - gyr_z_offset)  / -16.4 / 57.2957;// / 7505.75);
            //mag_x = static_cast<float>( mag_y_int );
            //mag_y = static_cast<float>( mag_x_int );
            //mag_z = -1.0f*static_cast<float>( mag_z_int );
            if (calc_offset == false)
            {
              //ahrs.Update(gyr_x, gyr_y, gyr_z, acc_x, acc_y, acc_z);
              ahrs.Lock();
              ahrs.Update(gyr_x,gyr_y,gyr_z,acc_x,acc_y,acc_z);
              //ahrs.Update(gyr_x, gyr_y, gyr_z, acc_x, acc_y, acc_z, mag_x, mag_y, mag_z);
              ahrs.Unlock();
            } else {
              zaehler++;
              acc_x_avr += acc_x_int;
              acc_y_avr += acc_y_int;
              acc_z_avr += acc_z_int - 2048;
              temp_avr += temp_int;
              gyr_x_avr += gyr_x_int;
              gyr_y_avr += gyr_y_int;
              gyr_z_avr += gyr_z_int;
              if (zaehler >= AVERAGE)
              {
                acc_x_avr /= AVERAGE;
                acc_y_avr /= AVERAGE;
                acc_z_avr /= AVERAGE;
                temp_avr  /= AVERAGE;
                gyr_x_avr /= AVERAGE;
                gyr_y_avr /= AVERAGE;
                gyr_z_avr /= AVERAGE;
                if (calc_offset)
                {
                  //acc_x_offset = acc_x_avr;
                  //acc_y_offset = acc_y_avr;
                  //acc_z_offset = acc_z_avr;
                  gyr_x_offset = gyr_x_avr;
                  gyr_y_offset = gyr_y_avr;
                  gyr_z_offset = gyr_z_avr;
                  ahrs.Lock();
                  ahrs.ResetOrientation();
                  ahrs.Unlock();
                  calc_offset = false;
                }
                zaehler = 0;
            }
            }
          }
        }
        else {
          //palTogglePad(GPIOD, 14);  //ROT
          modules::sensors::mpu.resetFIFO();
          //BaseThread::sleep(MS2ST(10));
          modules::sensors::mpu.setFIFOEnabled(true);
          //chprintf(reinterpret_cast<BaseSequentialStream *>(&SD3), "Odd fifo count: %u\n\r", count);
        }

      }
    }
    return RDY_OK;
  }

  StabilizeThread::StabilizeThread(void) : chibios_rt::BaseStaticThread<8192>() {
  }

  /*
   * Tester thread class. This thread executes the test suite.
   */

    msg_t BlinkerThread::main(void) {

      this->setName("Blinker");

      while (true) {
        palTogglePad(GPIOD, 15);//blau
        chibios_rt::BaseThread::sleep(MS2ST(500));
      }

      return RDY_OK;
    }

    BlinkerThread::BlinkerThread(void) : chibios_rt::BaseStaticThread<64>() {
      palSetPad(GPIOD, 15);
    }


    msg_t IdleThread::main(void)
    {
      this->setName("Idle");

      while (true) {
        if (!isUsbActive())
          threads::arm = false;
        if (threads::arm)
        {
          palSetPad(GPIOD, 14);
          palSetPad(GPIOD,  0);
        } else {
          palClearPad(GPIOD, 14);
          palClearPad(GPIOD,  0);
        }

        if (threads::calc_offset)
          palSetPad(GPIOD, 12); //grün
        else
          palClearPad(GPIOD, 12); //grün

        if (threads::print_orientation)
        {
          struct EulerAngles euler;
          ahrs.Lock();
          Quaternion q = ahrs.Orientation();
          ahrs.Unlock();
          euler = q.euler();
          chprintf(reinterpret_cast<BaseSequentialStream *>(&SD3), "euler: roll:%05.1f pitch:%05.1f yaw:%05.1f \r\n", euler.roll*57.296, euler.pitch*57.296, euler.yaw*57.296);
          chprintf(reinterpret_cast<BaseSequentialStream *>(&SD3), "quaternion: %.3f %.3f %.3f %.3f\r\n", q.w, q.x, q.y, q.z);
        }

        chibios_rt::BaseThread::sleep(MS2ST(100));
        }

      return RDY_OK;
    }

    IdleThread::IdleThread(void) : chibios_rt::BaseStaticThread<1024>()
    {
    }

    msg_t MotorsThread::main(void)
    {
      modules::PID::PIDlimits limits;
      limits.high = 255.0f;
      limits.low = -255.0f;

      pidRoll.Lock();
      pidPitch.Lock();
      pidYaw.Lock();
      pidRoll.OutputLimits(limits);
      pidPitch.OutputLimits(limits);
      pidYaw.OutputLimits(limits);
      pidRoll.WindUpGuard(100.0f); //8
      pidPitch.WindUpGuard(100.0f); //8
      pidYaw.WindUpGuard(100.0f); //8
      pidRoll.Unlock();
      pidPitch.Unlock();
      pidYaw.Unlock();

      while (true)
      {
        if (threads::arm)
        {
          float gain, tmpGas, pidRollOutput, pidPitchOutput, pidYawOutput, m1, m2, m3, m4;
          modules::MadgwickAHRS::AHRSAngleRates ahrsRates;
          gain = 1.0f;
          ahrs.Lock();
          EulerAngles currentAngles = ahrs.Orientation().euler();
          ahrsRates = ahrs.AngleRates();
          ahrs.Unlock();
          currentAngles.roll *= 57.296;
          currentAngles.pitch *= 57.296;
          currentAngles.yaw *= 57.296;

          switch (threads::stabilizeAngle)
          {
            case threads::StabilizeAngle::roll :     pidRoll.Lock();
                                                     pidPitch.Lock();
                                                     pidYaw.Lock();

                                                     pidRollOutput = pidRoll.Compute(currentAngles.roll, desiredAngles.roll);
                                                     pidPitchOutput = 0.0f;
                                                     pidPitch.Reset();
                                                     pidYawOutput = 0.0f;
                                                     pidYaw.Reset();

                                                     pidRoll.Unlock();
                                                     pidPitch.Unlock();
                                                     pidYaw.Unlock();

                                                     break;

            case threads::StabilizeAngle::pitch :    pidRoll.Lock();
                                                     pidPitch.Lock();
                                                     pidYaw.Lock();

                                                     pidPitchOutput = pidPitch.Compute(currentAngles.pitch, desiredAngles.pitch);
                                                     pidRollOutput = 0.0f;
                                                     pidRoll.Reset();
                                                     pidYawOutput = 0.0f;
                                                     pidYaw.Reset();

                                                     pidRoll.Unlock();
                                                     pidPitch.Unlock();
                                                     pidYaw.Unlock();

                                                     break;

            case threads::StabilizeAngle::yaw :      pidRoll.Lock();
                                                     pidPitch.Lock();
                                                     pidYaw.Lock();

                                                     pidRollOutput   = 0.0f;
                                                     pidRoll.Reset();
                                                     pidPitchOutput  = 0.0f;
                                                     pidPitch.Reset();
                                                     pidYawOutput    = pidYaw.Compute(currentAngles.yaw, desiredAngles.yaw);

                                                     pidRoll.Unlock();
                                                     pidPitch.Unlock();
                                                     pidYaw.Unlock();

                                                     break;

            case threads::StabilizeAngle::both :     pidRoll.Lock();
                                                     pidPitch.Lock();
                                                     pidYaw.Lock();

                                                     pidRollOutput =  pidRoll.Compute(currentAngles.roll, desiredAngles.roll);
                                                     pidPitchOutput = pidPitch.Compute(currentAngles.pitch, desiredAngles.pitch);
                                                     pidYawOutput = 0.0f;
                                                     pidYaw.Reset();

                                                     pidRoll.Unlock();
                                                     pidPitch.Unlock();
                                                     pidYaw.Unlock();

                                                     break;

            case threads::StabilizeAngle::all :      pidRoll.Lock();
                                                     pidPitch.Lock();
                                                     pidYaw.Lock();

                                                     pidRollOutput   = pidRoll.Compute(currentAngles.roll, desiredAngles.roll);
                                                     pidPitchOutput  = pidPitch.Compute(currentAngles.pitch, desiredAngles.pitch);
                                                     pidYawOutput    = pidYaw.Compute(currentAngles.yaw, desiredAngles.yaw);

                                                     pidRoll.Unlock();
                                                     pidPitch.Unlock();
                                                     pidYaw.Unlock();
                                                     break;
          }

          tmpGas = threads::gas;

          if (tmpGas < 130)
            gain = 2.0f;
          if (tmpGas < 80)
            gain = 3.0f;

          if (tmpGas < motors.min())
            tmpGas = motors.min();

          m1 = tmpGas - pidRollOutput*gain + pidPitchOutput*gain + pidYawOutput; //vorne rechts
          m2 = tmpGas + pidRollOutput*gain - pidPitchOutput*gain - pidYawOutput; //hinten links
          m3 = tmpGas - pidRollOutput*gain - pidPitchOutput*gain + pidYawOutput; //hinten rechts
          m4 = tmpGas + pidRollOutput*gain + pidPitchOutput*gain - pidYawOutput; //vorne links

          threads::motors.Lock();
          threads::motors.speed(m1,m2,m3,m4);
          threads::motors.Unlock();
        } else
        {
          pidRoll.Lock();
          pidPitch.Lock();
          pidYaw.Lock();
          pidRoll.Reset();
          pidPitch.Reset();
          pidYaw.Reset();
          pidRoll.Unlock();
          pidPitch.Unlock();
          pidYaw.Unlock();
        }
        //else
        //  threads::motors.speed(0,0,0,0); //do not use this code which is commented out. Problem is: motors will spin because of internal function of speed()
        chibios_rt::BaseThread::sleep(MS2ST(5));
      }
      return RDY_OK;
    }

    MotorsThread::MotorsThread(void) : chibios_rt::BaseStaticThread<2048>()
    {
    }

    msg_t BatteryThread::main(void)
    {
      while (true)
      {
        bms.Lock();
        bms.Update();
        bool state = bms.StateOk();
        bool tmp = false;
        bms.Unlock();
        if (!state)
        {
          tmp = true;
          buzzer.Lock();
        }
        while (!state)
        {
          buzzer.On();
          chibios_rt::BaseThread::sleep(MS2ST(500));
          buzzer.Off();
          chibios_rt::BaseThread::sleep(MS2ST(500));
          buzzer.On();
          chibios_rt::BaseThread::sleep(MS2ST(500));
          buzzer.Off();
          chibios_rt::BaseThread::sleep(MS2ST(500));
          bms.Lock();
          bms.Update();
          state = bms.StateOk();
          bms.Unlock();
        }
        if (tmp)
          buzzer.Unlock();
        chibios_rt::BaseThread::sleep(MS2ST(5000));
      }
      return RDY_OK;
    }

    BatteryThread::BatteryThread(void) : chibios_rt::BaseStaticThread<1024>()
    {

    }
}
