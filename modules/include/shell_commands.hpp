/*
 * shell_commands.hpp
 *
 *  Created on: 08.06.2014
 *      Author: tobias
 */

#ifndef SHELL_COMMANDS_HPP_
#define SHELL_COMMANDS_HPP_

#include "ch.hpp"
#include "hal.h"
#include "chprintf.h"
extern "C" {
  #include "shell.h"
  #include "myUSB.h"
}


namespace shell {

  extern Thread* th_shell;
  void cmd_calibrate(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_arm(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_gas(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_stabilize(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_print(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_pid(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_akku(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_buzzer(BaseSequentialStream *chp, int argc, char *argv[]);
  void cmd_angle(BaseSequentialStream *chp, int argc, char *argv[]);


  static const ShellCommand commands[] = {
                                          {"calibrate", static_cast<shellcmd_t>(cmd_calibrate)},
                                          {"arm", static_cast<shellcmd_t>(cmd_arm)},
                                          {"gas", static_cast<shellcmd_t>(cmd_gas)},
                                          {"stabilize", static_cast<shellcmd_t>(cmd_stabilize)},
                                          {"print", static_cast<shellcmd_t>(cmd_print)},
                                          {"pid", static_cast<shellcmd_t>(cmd_pid)},
                                          {"akku", static_cast<shellcmd_t>(cmd_akku)},
                                          {"buzzer", static_cast<shellcmd_t>(cmd_buzzer)},
                                          {"angle", static_cast<shellcmd_t>(cmd_angle)},
                                          {NULL, NULL}
  };

  static const ShellConfig config = {
                                     reinterpret_cast<BaseSequentialStream*>(&SDU1), commands
  };

}

#endif /* SHELL_COMMANDS_HPP_ */
