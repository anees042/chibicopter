/*
    AHRS Class Header - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Revisions:
 *  Date        Version         Author          Description
 *  25.04.2014  0.1             Rothfelder      First version
 */


/**
 * @file    AHRS.hpp
 * @brief   AHRS Class header file.
 *
 * @addtogroup AHRS
 * @{
 */

#ifndef AHRS_HPP_
#define AHRS_HPP_
#include "ch.hpp"
#include "hal.h"
#include "chprintf.h"
#include "quaternion.hpp"

namespace modules {
  class MadgwickAHRS
  {
    public:
      struct AHRSAngleRates {
        float roll;
        float pitch;
        float yaw;
      };


      MadgwickAHRS(float updateFreq);
      MadgwickAHRS(float updateFreq, float beta);
      void Update(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
      void Update(float gx, float gy, float gz, float ax, float ay, float az);
      Quaternion Orientation();
      void sampleFreq(float hz);
      float sampleFreq();
      void samplePeriod(float ms);
      float samplePeriod();
      void proportionalGain(float kp);
      float proportionalGain();
      AHRSAngleRates AngleRates(void);
      void ResetOrientation(void);
      void Lock(void);
      void Unlock(void);

    private:
      Quaternion m_orientation;
      float invSqrt(float x);
      float m_samplePeriod;  //in ms
      float m_beta;
      AHRSAngleRates m_rates;
      chibios_rt::BinarySemaphore m_semaphore;
  };
}


#endif /* IMU_HPP_ */
 /*! @} */
