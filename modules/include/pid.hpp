/*
    PID Class Header - Copyright (C) 2014 Tobias Rothfelder.

    This file is part of OpenQuadro.

    OpenQuadro is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    OpenQuadro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Revisions:
 *  Date        Version         Author          Description
 *  25.04.2014  0.1             Rothfelder      First version
 */


/**
 * @file    PID.hpp
 * @brief   PID Class header file.
 *
 * @addtogroup PID
 * @{
 */

#ifndef PID_HPP_
#define PID_HPP_

#include "ch.hpp"
#include "chconf.h"
#include "hal.h"
#include "chtypes.h"

namespace modules {

  class PID {
    public:
      enum class PIDdirection { NORMAL, REVERSE };
      struct PIDlimits { float high; float low; };
      struct PIDtunings { float kp; float ki; float kd;
                          PIDtunings()
                          {
                            this->kp = 0;
                            this->ki = 0;
                            this->kd = 0;
                          }
                          PIDtunings(float kp, float ki, float kd)
                          {
                            this->kp = kp;
                            this->ki = ki;
                            this->kd = kd;
                          }};

      PID(struct PIDtunings tunings, enum PIDdirection, float sampleFreq);
      PID(struct PIDtunings tunings, enum PIDdirection, float sampleFreq, float setpoint);
      PID(struct PIDtunings tunings, enum PIDdirection, float sampleFreq, struct PIDlimits limits);
      PID(struct PIDtunings tunings, enum PIDdirection, float sampleFreq, float setpoint, struct PIDlimits limits);
      float Compute(float input);
      float Compute(float input, float setpoint);
      void Tunings(struct PIDtunings newTunings);
      struct PIDtunings Tunings();
      void SamplePeriod(float ms);
      float SamplePeriod();
      void SampleFreq(float hz);
      float SampleFreq();
      void OutputLimits(struct PIDlimits newLimits);
      struct PIDlimits OutputLimits();
      void OutputLimitsActive(bool activate);
      bool OutputLimitsActive();
      void Direction(enum PIDdirection newDirection);
      enum PIDdirection Direction();
      void Kp(float kp);
      float Kp();
      void Ki(float ki);
      float Ki();
      void Kd(float kd);
      float Kd();
      void Setpoint(float setpoint);
      float Setpoint();
      float Output();
      void Reset();
      void WindUpGuard(float value);
      float WindUpGuard(void);
      void WindUpGuardActive(bool enabled);
      bool WindUpGuardActive(void);
      void Lock(void);
      void Unlock(void);

    protected:
    private:
      struct PIDtunings m_tunings;
      struct PIDtunings m_tuningsWeighted; //weighted by sampletime so sampletime is multiplicated or divided
      enum PID::PIDdirection m_direction;
      float m_sampleperiod;
      float m_setpoint;
      struct PIDlimits m_limits;
      bool m_limitoutputs;
      float m_integratedWeightedError; //integrated error * ki
      float m_lastInput;
      float m_output;
      float m_windupguard;
      bool m_windupguardactive;
      bool m_wasReset;
      chibios_rt::BinarySemaphore m_semaphore;
  };
}



#endif /* PID_HPP_ */
 /*! @} */
