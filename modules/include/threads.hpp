/*
 * threads.hpp
 *
 *  Created on: 08.06.2014
 *      Author: tobias
 */

#ifndef THREADS_HPP_
#define THREADS_HPP_

#include "ch.hpp"
#include "hal.h"
#include "chconf.h"
#include <pal.h>
#include "stm32f4xx.h"
#include "init_hw.h"
#include "chprintf.h"
#include "ahrs.hpp"
#include "quaternion.hpp"
#include "sensors.hpp"
#include "threads.hpp"
#include "pid.hpp"
#include "motorcontroller.hpp"
#include "bms.hpp"
#include "buzzer.hpp"

namespace threads {
  enum class StabilizeAngle {
      roll,pitch,yaw,both,all
  };
  extern StabilizeAngle stabilizeAngle;
  extern modules::MadgwickAHRS ahrs;
  extern bool arm;
  extern bool calc_offset;
  extern uint8_t gas;
  extern modules::PID pidPitch;
  extern modules::PID pidRoll;
  extern modules::PID pidYaw;
  extern driver::MotorController motors;
  extern bool print_orientation;
  extern modules::BMS bms;
  extern modules::Buzzer buzzer;
  extern EulerAngles desiredAngles;


  class StabilizeThread : public chibios_rt::BaseStaticThread<8192> {

  protected:
    virtual msg_t main(void);

  public:
    StabilizeThread(void);
  };

    /*
     * Tester thread class. This thread executes the test suite.
     */
  class BlinkerThread : public chibios_rt::BaseStaticThread<64> {

    protected:
      virtual msg_t main(void);
    public:
      BlinkerThread(void);
    };

  class IdleThread : public chibios_rt::BaseStaticThread<1024> {

    protected:
      virtual msg_t main(void);
    public:
      IdleThread(void);
    };

  class MotorsThread : public chibios_rt::BaseStaticThread<2048> {

  protected:
    virtual msg_t main(void);

  public:
    MotorsThread(void);
  };

  class BatteryThread : public chibios_rt::BaseStaticThread<1024> {

  protected:
    virtual msg_t main(void);

  public:
    BatteryThread(void);
  };
}



#endif /* THREADS_HPP_ */
