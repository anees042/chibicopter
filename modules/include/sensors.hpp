/*
 * sensors.hpp
 *
 *  Created on: 09.05.2014
 *      Author: tobias
 */

#ifndef SENSORS_HPP_
#define SENSORS_HPP_

#include "BMP085.hpp"
#include "HMC5883L.hpp"
#include "MPU6050.h"


namespace modules {

  namespace sensors {

    void SENSORS();
    extern MPU6050 mpu;
    extern BMP085 pressure;
    extern HMC5883L magnetometer;

  }

}



#endif /* SENSORS_HPP_ */
