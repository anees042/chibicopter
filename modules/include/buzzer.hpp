/*
 * buzzer.hpp
 *
 *  Created on: 13.06.2014
 *      Author: tobias
 */

#ifndef BUZZER_HPP_
#define BUZZER_HPP_

#include "ch.hpp"
#include "chconf.h"
#include "hal.h"
#include "pal.h"
#include "chtypes.h"

namespace modules {
  class Buzzer {
    public:
      Buzzer(void);
      void On(void);
      void Off(void);
      void Test(void);
      void Lock(void);
      void Unlock(void);

    private:
      chibios_rt::BinarySemaphore m_semaphore;
  };
}



#endif /* BUZZER_HPP_ */
