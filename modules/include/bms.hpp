/*
 * bms.hpp
 *
 *  Created on: 13.06.2014
 *      Author: tobias
 */

#ifndef BMS_HPP_
#define BMS_HPP_
#include "ch.hpp"
#include "hal.h"
#include "chprintf.h"
#include "init_hw.h"
#include "adc.h"

namespace modules {

  class BMS {
    public:
      BMS(const ADCConversionGroup* group, ADCDriver* adcd);
      BMS(const ADCConversionGroup* group, ADCDriver* adcd, float cell1Threshold, float cell2Threshold, float cell3Threshold);
      void Update(void);
      float Cell1(void);
      float Cell2(void);
      float Cell3(void);
      float Cell1Threshold(void);
      void Cell1Threshold(float newThreshold);
      float Cell2Threshold(void);
      void Cell2Threshold(float newThreshold);
      float Cell3Threshold(void);
      void Cell3Threshold(float newThreshold);
      bool StateOk(void);
      void Lock(void);
      void Unlock(void);

    protected:
    private:
      ADCDriver* m_adcd;
      float m_cell1;
      float m_cell2;
      float m_cell3;
      float m_cell1Threshold;
      float m_cell2Threshold;
      float m_cell3Threshold;
      float m_stateOk;
      const ADCConversionGroup* m_group;
      chibios_rt::BinarySemaphore m_semaphore;
  };
}


#endif /* BMS_HPP_ */
